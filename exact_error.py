import math
from error_measure import *

class exact_error(error_measure):

    # Class to compute exact model errors based on <optimization_problem>- and <dhn>-class.
    # Inherits from <error_measure>-class.

    def __init__(self, network_data, parameters, computational_parameters):
        super().__init__(network_data, parameters, computational_parameters)
        self.el_pipes = {}

    def evaluate_local_analytical_solution(self,pipe_id,requested_model_level,x):
        e_at_0 = self.energy_solution[(pipe_id,0)]
        if requested_model_level == 3:
            return e_at_0
        else:
            #Pipe specific parameters
            kw = self.network_data.edges[pipe_id]["ht_coefficient"]
            D = self.network_data.edges[pipe_id]["diameter"]
            lamb = math.pow(2*math.log(self.network_data.edges[pipe_id]["diameter"]/self.network_data.edges[pipe_id]["roughness"],10)+1.138,-2) 

            #General parameters
            Tw = self.parameters["temp_ground"]+273
            rho = self.parameters["rho"]

            #Solution dependent parameters
            vel = self.velocity_solution[pipe_id]

            #Analytical solution parameters
            alpha = -(4*kw*self.theta_state_equation[2])/(D*self.e0*self.e0)
            beta = -(4*kw*self.theta_state_equation[1])/(D*self.e0)
            zeta = vel
            is_model_one = (requested_model_level == 1)
            gamma = (lamb*rho*abs(vel)*vel*vel)/(2*D)*is_model_one - (4*kw*(self.theta_state_equation[0] - Tw))/D

            #Computing the analytical solution
            sqrt_term = math.sqrt(beta*beta - 4*alpha*gamma)
            term_one = sqrt_term/(2*alpha)
            term_two_upper = 1 + math.exp(x*sqrt_term/zeta)*(2*alpha*e_at_0 + beta - sqrt_term)/(2*alpha*e_at_0 + beta + sqrt_term)
            term_two_lower = 1 - math.exp(x*sqrt_term/zeta)*(2*alpha*e_at_0 + beta - sqrt_term)/(2*alpha*e_at_0 + beta + sqrt_term)
            term_two = term_two_upper/term_two_lower
            term_three = beta/(2*alpha)
            e_at_x = term_one*term_two - term_three
            return e_at_x
        
    def save_analytical_solution(self,pipe_id,requested_model_level):
        M =  self.network_data.get_pipe_M(pipe_id)
        new_delta_x = self.network_data.get_pipe_length(pipe_id)/(self.M_evaluation - 1)
        factor = (M-1)/(self.M_evaluation-1)
        for index in range(self.M_evaluation):
            self.el_pipes[(pipe_id,requested_model_level,index*factor)] = self.evaluate_local_analytical_solution(pipe_id,requested_model_level,new_delta_x*index)

    def load_solution(self,energy_solution,velocity_solution):
        super().load_solution(energy_solution,velocity_solution)
        self.is_analytical_computed = {(e,l):False for e in self.pipes_set for l in range(1,4)}
        
    def compute_local_model_error(self,pipe_id,requested_model_level,index):
        e1_at_x = self.el_pipes[(pipe_id,1,index)]
        el_at_x = self.el_pipes[(pipe_id,requested_model_level,index)]
        return abs(e1_at_x - el_at_x)

    def compute_local_discretization_error(self,pipe_id,index):
        requested_model_level = self.network_data.get_pipe_level(pipe_id)
        el_at_x = self.el_pipes[(pipe_id,requested_model_level,index)]
        el_disc_at_x = self.energy_solution[(pipe_id,index)]
        return abs(el_at_x - el_disc_at_x)

    def compute_local_total_error(self,pipe_id,index):
        e1_at_x = self.el_pipes[(pipe_id,1,index)]
        el_disc_at_x = self.energy_solution[(pipe_id,index)]
        return abs(e1_at_x - el_disc_at_x)

    def compute_model_error(self,pipe_id,requested_model_level):
        
        if (requested_model_level) > 1:
            # Check if solution already exists.
            if (not self.is_analytical_computed[(pipe_id,requested_model_level)]):
                self.save_analytical_solution(pipe_id,requested_model_level)
                self.is_analytical_computed[(pipe_id,requested_model_level)] = True
            if (not self.is_analytical_computed[(pipe_id,1)]):
                self.save_analytical_solution(pipe_id,1)
                self.is_analytical_computed[(pipe_id,1)] = True
        
            # Compute max.
            highest_error = 0
            M =  self.network_data.get_pipe_M(pipe_id)
            factor = (M-1)/(self.M_evaluation-1)
            saved = np.zeros((self.M_evaluation,))
            for index in range(1,self.M_evaluation):
                local_model_error = self.compute_local_model_error(pipe_id,requested_model_level,index*factor)
                highest_error = max(highest_error,local_model_error)
                saved[index] = local_model_error
            #print(saved)
            return highest_error
        else:
            return 0.0

    def compute_discretization_error(self,pipe_id):

        current_pipe_level = self.network_data.get_pipe_level(pipe_id)
        
        if (current_pipe_level) < 3:        
            requested_model_level = self.network_data.get_pipe_level(pipe_id)
            if (not self.is_analytical_computed[(pipe_id,requested_model_level)]):
                self.save_analytical_solution(pipe_id,requested_model_level)
                self.is_analytical_computed[(pipe_id,requested_model_level)] = True
            
            highest_error = 0
            M =  self.network_data.get_pipe_M(pipe_id)
            factor = (M-1)/(self.M_evaluation-1)
            for index in range(1,self.M_evaluation):
                highest_error = max(highest_error,self.compute_local_discretization_error(pipe_id,index*factor))
            return highest_error
        
        else:
            return 0.0


    def compute_total_error(self,pipe_id):

        if (not self.is_analytical_computed[(pipe_id,1)]):
            self.save_analytical_solution(pipe_id,1)
            self.is_analytical_computed[(pipe_id,1)] = True

        highest_error = 0
        M =  self.network_data.get_pipe_M(pipe_id)
        factor = (M-1)/(self.M_evaluation-1)
        for index in range(1,self.M_evaluation):
            highest_error = max(highest_error,self.compute_local_total_error(pipe_id,index*factor))
        return highest_error
