from networkx import *
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

#We set up a class for DHN instances
class dhn(DiGraph):
    #Constructor that creates a networkx graph and some object variables
    def __init__(self,network_dict,consumer_info_dict,computational_parameters):
        ##Define relevant functions and adapt some values in order to get the right units##
        #Define function that removes 'id' key in dictionnary
        #and returns that new dictionary
        def nodes_without_id(d):
            new_d = d.copy()
            new_d.pop('id')
            return new_d
        #We do the same for the 'id', 'start_id' and 'end_id' and we add the slope
        #to the dictionnary
        def pipe_without_id(d,given_nodes,is_consumer=False,is_source=False):
            new_d = d.copy()
            new_d.pop('start_node_id')
            new_d.pop('end_node_id')
            new_d['is_consumer'] = is_consumer
            if not (is_consumer or is_source):
                for n in given_nodes:
                    if n["id"] == d['start_node_id']:
                        start_z = n['position'][2]
                    elif n["id"] == d['end_node_id']:
                        end_z = n['position'][2]
                new_d['slope'] = (end_z-start_z)/d['length']
                new_d['level'] = 3
                new_d['M'] = 2*computational_parameters["M_evaluation"] - 1
            else:
                new_d['M'] = 2
            new_d['is_source'] = is_source
            new_d['is_pipe'] = not (is_source or is_consumer)
            return new_d
        def merge_dicts(dict1, dict2):
            return(dict2.update(dict1))
        #Merge info of the relevant dictionnaries for the consumers
        for c1 in network_dict['consumers']:
            for c2 in consumer_info_dict:
                if c1['id'] == c2['id']:
                    merge_dicts(c2,c1)
        #Create nodes, pipes and consumers for the graph definition
        #and create the usefull class variables
        for p in network_dict['pipes']:
            p['diameter'] = p['diameter']/1e3
            p['roughness'] = p['roughness']/1e3

        # Save network name, if any.
        def get_name(network_dict):

            name = ""
            keys = list(network_dict.keys())
            for key in keys:
                if key == "name":
                    name = network_dict["name"]
                    break

            return name

        ##Create the graph with the relevant attributes##
        nodes = [(n['id'],nodes_without_id(n)) for n in network_dict['nodes']]
        nodes_ids = [n['id'] for n in network_dict['nodes']]
        pipes = [(p['start_node_id'],p['end_node_id'],pipe_without_id(p,network_dict['nodes'])) for p in network_dict['pipes']]
        pipes_ids = [(p['start_node_id'],p['end_node_id']) for p in network_dict['pipes']]
        consumers = [(c['start_node_id'],c['end_node_id'],pipe_without_id(c,network_dict['nodes'],is_consumer=True)) for c in network_dict['consumers']]
        consumers_ids = [(c['start_node_id'],c['end_node_id']) for c in network_dict['consumers']]
        sources = [(s['start_node_id'],s['end_node_id'],pipe_without_id(s,network_dict['nodes'],is_source=True)) for s in network_dict['sources']]
        sources_ids = [(s['start_node_id'],s['end_node_id']) for s in network_dict['sources']]
        super().__init__()
        super().add_nodes_from(nodes)
        super().add_edges_from(pipes)
        super().add_edges_from(consumers)
        super().add_edges_from(sources)
        self.pipes_ids = pipes_ids
        self.consumers_ids = consumers_ids
        self.sources_ids = sources_ids
        self.name = get_name(network_dict)

        ##Foreflow and backflow detection##
        #Initialize
        for n in self.nodes:
            self.nodes[n]["in_backflow"] = False
            self.nodes[n]["in_foreflow"] = False
        temp_G = Graph()
        temp_G.add_nodes_from(nodes_ids)
        temp_G.add_edges_from(pipes_ids)
        visited_nodes_backflow = [s[0] for s in sources_ids]
        visited_nodes_foreflow = [s[1] for s in sources_ids]
        for s in sources_ids:
            self.nodes[s[0]]["in_backflow"] = True
            self.nodes[s[1]]["in_foreflow"] = True

        #Apply depth first search
        neighbors_available = True
        while neighbors_available:
            neighbors_available = False
            for n1 in visited_nodes_backflow:
                for n2 in temp_G.neighbors(n1):
                    if n2 not in visited_nodes_backflow:
                        visited_nodes_backflow.append(n2)
                        self.nodes[n2]["in_backflow"] = True
                        neighbors_available = True

        neighbors_available = True
        while neighbors_available:
            neighbors_available = False
            for n1 in visited_nodes_foreflow:
                for n2 in temp_G.neighbors(n1):
                    if n2 not in visited_nodes_foreflow:
                        visited_nodes_foreflow.append(n2)
                        self.nodes[n2]["in_foreflow"] = True
                        neighbors_available = True



    #We define getters in order to esily acces the network info
    def get_pipes(self):
        return [(e[0],e[1],self.edges[e]) for e in self.edges if self.edges[e]["is_source"] != True and self.edges[e]["is_consumer"] != True]

    def get_consumers(self):
        return [(e[0],e[1],self.edges[e]) for e in self.edges if self.edges[e]["is_consumer"]]

    def get_sources(self):
        return [(e[0],e[1],self.edges[e]) for e in self.edges if self.edges[e]["is_source"]]

    def get_slopes(self):
        for e in self.edges:
            print((e[0],e[1],self.edges[e]))

    def get_pipes_ids(self):
        return self.pipes_ids

    def get_consumers_ids(self):
        return self.consumers_ids

    def get_sources_ids(self):
        return self.sources_ids

    def get_len_pipes(self):
        return len(self.pipes_ids)

    def get_len_consumers(self):
        return len(self.consumers_ids)

    def get_len_sources(self):
        return len(self.sources_ids)

    def get_pipe_length(self,e):
        return self.edges[e]["length"]

    def get_pipe_level(self,e):
        return self.edges[e]["level"]

    def get_pipe_delta_x(self,e):
        return self.edges[e]["length"]/(self.edges[e]["M"]-1)

    def get_pipe_M(self,e):
        return self.edges[e]["M"]

    def set_pipe_level(self,e,new_level):
        self.edges[e]["level"] = new_level

    def set_pipe_M(self,e,new_M):
        self.edges[e]["M"] = new_M

    def display_pipes_network_parameters(self):
        print("")
        print("\033[4mPipe: M,level\033[0m")
        for e in self.pipes_ids:
            print(str(e) + ": " + str(self.edges[e]["M"]) + "," + str(self.edges[e]["level"]))
        print("")    

    @staticmethod
    def diff(first, second):
        second = set(second)
        return [item for item in first if item not in second]
    
    def get_graph_without_consumers_depot(self):
        #Initialize necessary stuff
        set_A_pos = self.get_sources_ids() + self.get_consumers_ids()
        G_hat = DiGraph()
        G_hat.add_edges_from(self.get_pipes_ids())
        return G_hat
        
    #Method that applies the flow presolve algorithm
    def flow_direction_presolve(self):
        #Initialize necessary stuff
        set_A_pos = self.get_sources_ids() + self.get_consumers_ids()
        G_hat = Graph()
        G_hat.add_edges_from(self.diff(self.edges,set_A_pos))
        def merge_dicts(dict1, dict2):
            return(dict2.update(dict1))
        link_new_G_hat_edges_to_G_edges = {(e[1],e[0]):e for e in self.edges}
        merge_dicts({e:e for e in self.edges},link_new_G_hat_edges_to_G_edges)
        #Compute 2-connected components
        components = sorted(map(sorted, k_edge_components(G_hat,k=2)))
        #Create dicts that link the nodes from Ghat to the new nodes of Ghat
        link_G_to_new_G_hat = {components[c][n]:c+1 for c in range(0,len(components)) for n in range(len(components[c]))}
        #Create new edges
        new_edges = []
        link_new_G_hat_edges_to_G_edges_pos = {}
        link_new_G_hat_edges_to_G_edges_neg = {}
        for e in self.diff(self.edges,set_A_pos):
            link_e0 = link_G_to_new_G_hat[e[0]]
            link_e1 = link_G_to_new_G_hat[e[1]]
            if link_e0 != link_e1:
                new_edges.append((link_e0,link_e1))
                link_new_G_hat_edges_to_G_edges_pos[(link_e0,link_e1)] = e
                link_new_G_hat_edges_to_G_edges_neg[(link_e1,link_e0)] = e
        #Create new Ghat
        new_G_hat = Graph()
        new_G_hat.add_edges_from(new_edges)
        visited_nodes_foreflow = [link_G_to_new_G_hat[s[1]] for s in self.get_sources_ids()]
        visited_edges_foreflow = []
        still_nodes_to_add = True
        while still_nodes_to_add:
            still_nodes_to_add = False
            for n1 in visited_nodes_foreflow:
                for n2 in new_G_hat.neighbors(n1):
                    if n2 not in visited_nodes_foreflow:
                        visited_nodes_foreflow.append(n2)
                        visited_edges_foreflow.append((n1,n2))
                        still_nodes_to_add = True
        visited_nodes_backflow = [link_G_to_new_G_hat[s[0]] for s in self.get_sources_ids()]
        visited_edges_backflow = []
        still_nodes_to_add = True
        while still_nodes_to_add:
            still_nodes_to_add = False
            for n1 in visited_nodes_backflow:
                for n2 in new_G_hat.neighbors(n1):
                    if n2 not in visited_nodes_backflow:
                        visited_nodes_backflow.append(n2)
                        visited_edges_backflow.append((n1,n2))
                        still_nodes_to_add = True

        for e in self.edges:
            if self.edges[e]["is_consumer"] or self.edges[e]["is_source"]:
                self.edges[e]["flow_is_fixed_pos"] = True
            else:
                self.edges[e]["flow_is_fixed_pos"] = False
            self.edges[e]["flow_is_fixed_neg"] = False

        for s in set_A_pos:
            self.edges[s]["flow_is_fixed_pos"] = True

        for new_G_hat_e in visited_edges_foreflow:
            if new_G_hat_e in link_new_G_hat_edges_to_G_edges_pos.keys():
                self.edges[link_new_G_hat_edges_to_G_edges_pos[new_G_hat_e]]["flow_is_fixed_pos"] = True
            elif new_G_hat_e in link_new_G_hat_edges_to_G_edges_neg.keys():
                self.edges[link_new_G_hat_edges_to_G_edges_neg[new_G_hat_e]]["flow_is_fixed_neg"] = True
        for new_G_hat_e in visited_edges_backflow:
            if new_G_hat_e in link_new_G_hat_edges_to_G_edges_pos.keys():
                self.edges[link_new_G_hat_edges_to_G_edges_pos[new_G_hat_e]]["flow_is_fixed_neg"] = True
            elif new_G_hat_e in link_new_G_hat_edges_to_G_edges_neg.keys():
                self.edges[link_new_G_hat_edges_to_G_edges_neg[new_G_hat_e]]["flow_is_fixed_pos"] = True

        for e in self.edges:
            self.edges[e]["flow_is_free"] =  not (self.edges[e]["flow_is_fixed_pos"] or self.edges[e]["flow_is_fixed_neg"])

    #Method that plots the district heating network
    def plot_network(self, plot_basic = False):
        no_height_given = True
        extra_height = 0
        max_distance = 0
        text_size = 6
        node_size = 200
        x = [self.nodes[n]["position"][0] for n in self.nodes]
        y = [self.nodes[n]["position"][1] for n in self.nodes]
        max_x = max(x)
        max_y = max(y)
        min_x = min(x)
        min_y = min(y)
        max_diff = max([max_x-min_x,max_y-min_y])
        factor = 1/500
        z = [self.nodes[n]["position"][2]-max_diff*factor*no_height_given*self.nodes[n]["in_backflow"] for n in self.nodes]
        z_for_lines = [[self.nodes[n1]["position"][2]-max_diff*factor*no_height_given*self.nodes[n1]["in_backflow"],self.nodes[n2]["position"][2]-max_diff*factor*no_height_given*self.nodes[n2]["in_backflow"]] for n1,n2 in self.edges]
        x_for_lines = [[self.nodes[n1]["position"][0],self.nodes[n2]["position"][0]] for n1,n2 in self.edges]
        y_for_lines = [[self.nodes[n1]["position"][1],self.nodes[n2]["position"][1]] for n1,n2 in self.edges]
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        for n in self.nodes:
            x_pos = self.nodes[n]["position"][0]
            y_pos = self.nodes[n]["position"][1]
            z_pos = self.nodes[n]["position"][2]-max_diff*factor*no_height_given*self.nodes[n]["in_backflow"]
            ax.text(x_pos,y_pos,z_pos,n, color='white', ha='center', va='center', size=text_size, zorder=10)

        ax.scatter(x, y, z, s=node_size, marker='o', color='black', alpha = 1)
        if plot_basic:
            for i in range(len(z_for_lines)):
                ax.plot(x_for_lines[i],y_for_lines[i],z_for_lines[i])
        #fig.show()
        return node_size, no_height_given, max_diff,factor, text_size, ax, fig
