#SETS
self.m.V = Set()
self.m.X = Set(dimen = 3) #Indexed set using tuples (a,x), a is arc and x is discretization postion (in [0,La])

#VARIABLE
self.m.e_edge = Var(self.m.X, within = NonNegativeReals, bounds = (0.1,0.55))
self.m.p_node = Var(self.m.V, within = NonNegativeReals, bounds = (0,12))

#INITIALIZE DATA
data_dict["X"] = {None: [(a,x) for a in self.network.edges for x in range(0,self.network.edges[a]["M"])]}
data_dict["V"] = {None:list(self.network.nodes)}
