import math
import matplotlib
from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt
import numpy as np
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb}')
plt.rc('pgf', preamble=r'\usepackage{amsmath} \usepackage{amssymb}')

#Pipe parameters and state equation parameters
lamb = 0.017
D = 0.107
kw = 0.5
rho = 997
Tw = 278
e0 = 1e9
theta0 = 274.93729
theta1 = 220.536
theta2 = 59.2453
L = 1000

#Creating mesh
max_v = 0.006
min_v = 0.0001
v_pts = 1000
min_eat0 = 0.018050102989429957*1e9
max_eat0 = 0.5139720177640805*1e9
eat0_pts = 100
v_iter_plot = np.linspace(min_v,max_v,v_pts)
eat0_iter_plot = np.linspace(min_eat0,max_eat0,eat0_pts)
v, eat0 = np.meshgrid(v_iter_plot,eat0_iter_plot)
Tat0_iter_plot = theta2*((eat0_iter_plot/1e9)**2) + theta1*(eat0_iter_plot/1e9) + theta0


#Constants for the plot data
a  = -4*kw*theta2/D/e0/e0
b = -4*kw*theta1/D/e0
c = lamb*rho*(v*v)*np.absolute(v)/2/D-4*kw*(theta0-Tw)/D
absval = np.absolute((2*a*eat0+b+np.sqrt(b*b-4*a*c))/(2*a*eat0+b-np.sqrt(b*b-4*a*c)))

#Computing the plot data
C2 = (2*a*eat0+b-np.sqrt(b*b-4*a*c))/(2*a*eat0+b+np.sqrt(b*b-4*a*c))
eLa3 = (np.sqrt(b*b-4*a*c)/(2*a)*(1+C2*np.exp(L*np.sqrt(b*b-4*a*c)/v))/(1-C2*np.exp(L*np.sqrt(b*b-4*a*c)/v))-b/(2*a))/1e9
TLa3 = theta2*(eLa3**2) + theta1*(eLa3) + theta0

v, Tat0 = np.meshgrid(v_iter_plot,Tat0_iter_plot)


#Plot itself
fig8 = plt.figure()
ax8 = plt.axes(projection='3d')
ax8.plot_surface(v,Tat0,TLa3, cmap='inferno')
ax8.set(xlabel= "$v_a$", ylabel= '$T_a(0)$', zlabel= '$T_a(L_a)$')
ax8.xaxis.set_tick_params(labelsize=9)
ax8.yaxis.set_tick_params(labelsize=9)
ax8.zaxis.set_tick_params(labelsize=9)
ax8.view_init(azim = -130, elev = 25)
fig8.savefig('/home/marius/Documents/trier/thesis/manuscript/main-matter/figures/plots/zoom-fun-MR2.pgf', bbox_inches='tight')

plt.show()
