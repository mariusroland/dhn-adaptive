import math
import matplotlib
from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt
import numpy as np
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb}')
plt.rc('pgf', preamble=r'\usepackage{amsmath} \usepackage{amssymb}')

#Test with fixed coefficients, easy example
lamb = 0.017
D = 0.107
kw = 0.5
rho = 997
v = -8
Tw = 278
e0 = 1e9
theta0 = 274.93729
theta1 = 220.536
theta2 = 59.2453
eat0 = 0.35*1e9
L = 1000
Nb_pts = 10000
deltax = L/Nb_pts

a = -4*kw*theta2/D/e0/e0
b = -4*kw*theta1/D/e0
c = lamb*rho*abs(v)*(v**2)/2/D-4*kw*(theta0-Tw)/D

x = [0 for i in range(Nb_pts)]
e1_iter = [0 for i in range(Nb_pts)]
e2_iter = [0 for i in range(Nb_pts)]

absval = abs((2*a*eat0+b+math.sqrt(b**2-4*a*c))/(2*a*eat0+b-math.sqrt(b**2-4*a*c)))

temp = 0
for i in range(Nb_pts):
    x[i] = temp
    e1 = (math.sqrt(b**2-4*a*c)/(2*a)*(absval-math.exp(temp*math.sqrt(b**2-4*a*c)/v))/(absval+math.exp(temp*math.sqrt(b**2-4*a*c)/v))-b/(2*a))/1e9
    e2 = (math.sqrt(b**2-4*a*c)/(2*a)*(absval+math.exp(temp*math.sqrt(b**2-4*a*c)/v))/(absval-math.exp(temp*math.sqrt(b**2-4*a*c)/v))-b/(2*a))/1e9
    found = False
    e1_iter[i] = e1
    e2_iter[i] = e2
    temp = temp + deltax

    
fig1, ax1 = plt.subplots()
ax1.plot(x, e1_iter)
ax1.set(xlabel='x', ylabel='e1 (GJ/m3)')
ax1.grid()

fig2, ax2 = plt.subplots()
ax2.plot(x, e2_iter)
ax2.set(xlabel='x', ylabel='e2 (GJ/m3)')
ax2.grid()

#Test with changing coefficients
max_v = 0.0001
min_v = 8
v_pts = 100
min_eat0 = 0.2*1e9
max_eat0 = 0.5*1e9
eat0_pts = 25
v_iter_plot = np.linspace(min_v,max_v,v_pts)
eat0_iter_plot = np.linspace(min_eat0,max_eat0,eat0_pts)
v, eat0 = np.meshgrid(v_iter_plot,eat0_iter_plot)

a  = -4*kw*theta2/D/e0/e0
b = -4*kw*theta1/D/e0
c = lamb*rho*(v*v)*np.absolute(v)/2/D-4*kw*(theta0-Tw)/D
absval = np.absolute((2*a*eat0+b+np.sqrt(b*b-4*a*c))/(2*a*eat0+b-np.sqrt(b*b-4*a*c)))

eLa1 = (np.sqrt(b*b-4*a*c)/(2*a)*(absval-np.exp(L*np.sqrt(b*b-4*a*c)/v))/(absval+np.exp(L*np.sqrt(b*b-4*a*c)/v))-b/(2*a))/1e9
eLa2 = (np.sqrt(b*b-4*a*c)/(2*a)*(absval+np.exp(L*np.sqrt(b*b-4*a*c)/v))/(absval-np.exp(L*np.sqrt(b*b-4*a*c)/v))-b/(2*a))/1e9
eLa_final = np.zeros(eLa1.shape)
for r in range(eLa_final.shape[0]):
    for c in range(eLa_final.shape[1]):
        new_v = v_iter_plot[c]
        new_eat0 = eat0_iter_plot[r]
        new_a = -4*kw*theta2/D/e0/e0
        new_b = -4*kw*theta1/D/e0
        new_c = lamb*rho*(new_v*new_v)*abs(new_v)/2/D-4*kw*(theta0-Tw)/D
        junc_fun = 2*new_a*new_eat0 + new_b + math.sqrt(new_b**2-4*new_a*new_c)
        if junc_fun >= 0:
            eLa_final[r,c] = eLa1[r,c]
        elif junc_fun < 0:
            eLa_final[r,c] = eLa2[r,c]

#Test where no attention has been made for absolute value, as in forum posts
C2 = (2*a*eat0+b-np.sqrt(b*b-4*a*c))/(2*a*eat0+b+np.sqrt(b*b-4*a*c))
eLa3 = (np.sqrt(b*b-4*a*c)/(2*a)*(1+C2*np.exp(L*np.sqrt(b*b-4*a*c)/v))/(1-C2*np.exp(L*np.sqrt(b*b-4*a*c)/v))-b/(2*a))/1e9


fig4 = plt.figure()
ax4 = plt.axes(projection='3d')
ax4.plot_surface(v,eat0/1e9,eLa1, cmap='inferno')
ax4.set(xlabel= "$v_a$", ylabel= '$e_a(0)$', zlabel= '$e_a(L_a)$')

fig5 = plt.figure()
ax5 = plt.axes(projection='3d')
ax5.plot_surface(v,eat0/1e9,eLa2, cmap='inferno')
ax5.set(xlabel= "$v_a$", ylabel= '$e_a(0)$', zlabel= '$e_a(L_a)$')

fig6 = plt.figure()
ax6 = plt.axes(projection='3d')
ax6.w_xaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
ax6.w_yaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
ax6.w_zaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
ax6.plot_surface(v,eat0/1e9,eLa_final, cmap='inferno', edgecolor='none')
ax6.set_xlabel("$v_a$", fontsize = 12)
ax6.set_ylabel('$e_a(0)$', fontsize = 12)
ax6.set_zlabel('$e_a(L_a)$', fontsize = 12)
ax6.xaxis.set_tick_params(labelsize=9)
ax6.yaxis.set_tick_params(labelsize=9)
ax6.zaxis.set_tick_params(labelsize=9)
ax6.view_init(azim = -130, elev = 25)
#fig6.savefig('/home/marius/Documents/trier/dhn/dhn-adaptive/paper/figures/pgf/energy_solution_positive_velocity.pgf', bbox_inches='tight')

fig7 = plt.figure()
ax7 = plt.axes(projection='3d')
ax7.plot_surface(v,eat0/1e9,eLa1, cmap='inferno')
ax7.plot_surface(v,eat0/1e9,eLa2, cmap='inferno')
ax7.set(xlabel= "$v_a$", ylabel= '$e_a(0)$', zlabel= '$e_a(L_a)$')

fig8 = plt.figure()
ax8 = plt.axes(projection='3d')
ax8.plot_surface(v,eat0/1e9,eLa3, cmap='inferno')
ax8.set(xlabel= "$v_a$", ylabel= '$e_a(0)$', zlabel= '$e_a(L_a)$')
ax8.xaxis.set_tick_params(labelsize=9)
ax8.yaxis.set_tick_params(labelsize=9)
ax8.zaxis.set_tick_params(labelsize=9)
ax8.view_init(azim = -130, elev = 25)
#fig8.savefig('/home/marius/Documents/trier/dhn/dhn-adaptive/paper/figures/pgf/energy_solution_negative_velocity.pgf', bbox_inches='tight')

plt.show()
