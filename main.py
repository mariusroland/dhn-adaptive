from optimization_problem import *
from exact_error import *
from adaptive_algorithm import *
import json
# 

# Provide file locations.
network_name = "street" # Aroma_simplified3
system_file = "src/network_data/" + network_name + "_system.json" # "src/network_data/Aroma_system.json"
scenario_file = "src/network_data/" + network_name + "_scenario.json"
parameters_file = "src/parameters/parameters.json"
computation_parameters_file = "src/parameters/computational_parameters.json"
plots_save_file = "../paper/figures/pgf/exact-error/" + network_name

def import_json(file):
    with open(file) as json_file:
        return json.load(json_file)
# Load data from files as json.
system_data = import_json(system_file)
system_data["name"] = network_name
scenario_data = import_json(scenario_file)
parameters_data = import_json(parameters_file)
computational_parameters_data = import_json(computation_parameters_file)

# computational_parameters_data["measure"] = "exact"
# computational_parameters_data["measure"] = "estimate"


adaptive_algorithm = adaptive_algorithm(system_data,scenario_data,parameters_data,computational_parameters_data)
adaptive_algorithm.set_plot_file(plots_save_file)
adaptive_algorithm.apply_adaptive_algorithm(plot_network_flag=True)

# adaptive_algorithm.plot_error_values()
# adaptive_algorithm.plot_inner_loop_sets()
# adaptive_algorithm.plot_outer_loop_sets()
# adaptive_algorithm.plot_boxplot_Ms()
# adaptive_algorithm.plot_solving_times()
# adaptive_algorithm.plot_model_level_sets_sizes()

adaptive_algorithm.plot_error_values(plots_save_file)
adaptive_algorithm.plot_inner_loop_sets(plots_save_file)
adaptive_algorithm.plot_outer_loop_sets(plots_save_file)
adaptive_algorithm.plot_boxplot_Ms(plots_save_file)
adaptive_algorithm.plot_solving_times(plots_save_file)
adaptive_algorithm.plot_model_level_sets_sizes(plots_save_file)
adaptive_algorithm.plot_inner_outer_loop_sets(plots_save_file)

# input()
