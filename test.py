from optimization_problem import *
from dhn import *
from exact_error import *
from adaptive_algorithm import *
import json

# Provide file locations.
network_name = "Kolibri" # Aroma_simplified3
system_file = "src/network_data/" + network_name + "_system.json" # "src/network_data/Aroma_system.json"
scenario_file = "src/network_data/" + network_name + "_scenario.json"
parameters_file = "src/parameters/parameters.json"
computation_parameters_file = "src/parameters/computational_parameters.json"
plots_save_file = "../paper/figures/pgf/exact-error/" + network_name

def import_json(file):
    with open(file) as json_file:
        return json.load(json_file)
# Load data from files as json.
system_data = import_json(system_file)
system_data["name"] = network_name
scenario_data = import_json(scenario_file)
parameters_data = import_json(parameters_file)
computational_parameters_data = import_json(computation_parameters_file)

network = dhn(system_data,scenario_data,computational_parameters_data)
for pipe_id in network.get_pipes_ids():
    network.set_pipe_level(pipe_id,3)
    network.set_pipe_M(pipe_id,2)
network.flow_direction_presolve()
    
optimizaton_problem = optimization_problem(network,parameters_data,computational_parameters_data)
optimizaton_problem.setup_intance()

optimizaton_problem.setup_intance()
optimizaton_problem.initial_warmstart()
optimizaton_problem.solve()
#optimizaton_problem.plot_node_results()
input()
