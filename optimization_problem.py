from pyomo.environ import *
from dhn import *
import math
import time
import copy
from pyomo.opt import SolverFactory
from networkx.linalg.graphmatrix import incidence_matrix
import numpy as np
import csv
import json
np.set_printoptions(threshold=np.inf)

class optimization_problem:
    def __init__(self,network,parameters,computational_parameters):

        # First saving the instance data
        self.network = network
        self.parameters = parameters
        self.computational_parameters = computational_parameters

        # Computational sets
        self.nlp_solving_time = []

        ###Initialize the abstract model###
        self.m = AbstractModel()

        ###Initialize the sets###
        self.m.Ap = Set(dimen = 2)
        self.m.Ac = Set(dimen = 2)
        self.m.Ad = Set(dimen = 2)
        self.m.A = self.m.Ap | self.m.Ac | self.m.Ad
        self.m.V = Set()
        self.m.X = Set(dimen = 3) #Indexed set using tuples (a,x) !!!Has only boundary tuples for the consumer and depot arcs
        self.m.X_pipe = Set(dimen = 3) #Similar as X but no elements for a in consumers and depot
        self.m.X_pipe_T = Set(dimen = 3) #Same as X_pipe but if we are in level 3 the set is empty
        self.m.X_pipe_bounds = Set(dimen = 3) #Same as X but only boundary 0 and M for all pipes that have no fixed velocity
        self.m.delta_out = Set(dimen = 3) #Indexed set using tuples (u,a) if arc a is outgoing
        self.m.delta_in = Set(dimen = 3) #Indexed set using tuples (u,a) if a is ingoing
        self.m.pos_vel = Set(dimen = 2) #Set of positive oriented arcs
        self.m.neg_vel = Set(dimen = 2) #Set of negative oriented arcs
        self.m.no_fixed_vel = self.m.Ap - self.m.pos_vel - self.m.neg_vel #Set of negative oriented arcs
        self.m.two_constraints = Set(initialize = [1,2])
        self.m.three_constraints = Set(initialize = [1,2,3])
        self.m.four_constraints = Set(initialize = [1,2,3,4])
        self.m.five_constraints = Set(initialize = [1,2,3,4,5])
        self.m.six_constraints = Set(initialize = [1,2,3,4,5,6])
        self.m.seven_constraints = Set(initialize = [1,2,3,4,5,7])

        ###Initialize the parameters###
        self.m.upper_bound_on_Pg = Param()
        self.m.upper_bound_on_Pp = Param()
        self.m.M = Param(self.m.A) #How many discrete points considered starting from 0 for all edges !!!For Depot and Cosumers M = 2
        self.m.l = Param(self.m.Ap) #Level in the hierarchy
        self.m.L = Param(self.m.Ap)
        self.m.delta_x = Param(self.m.Ap)
        self.m.D = Param(self.m.Ap)
        self.m.Area = Param(self.m.Ap)
        self.m.h = Param(self.m.Ap)
        self.m.lamb = Param(self.m.Ap)
        self.m.k = Param(self.m.Ap)
        self.m.Pc = Param(self.m.Ac)
        self.m.eff = Param(self.m.Ac)
        self.m.ebf = Param(self.m.Ac)
        self.m.rho = Param(self.m.A)
        self.m.consumer_outlet_temperature_epsilon = Param()
        self.m.stagnation_pressure_epsilon = Param()
        self.m.Tw = Param()
        self.m.ps = Param()
        self.m.g = Param()
        self.m.Cw = Param()
        self.m.Cg = Param()
        self.m.Cp = Param()
        self.m.p_plus = Param(self.m.V)
        self.m.p_minus = Param(self.m.V)
        self.m.e_plus = Param(self.m.V)
        self.m.e_minus = Param(self.m.V)
        self.m.Pw_plus = Param()
        self.m.Pg_plus = Param()
        self.m.Pp_plus = Param()
        self.m.hours_in_day = Param()
        
        ####Initialize variables###
        self.m.q = Var(self.m.A, bounds = (-20,20))
        self.m.p_node = Var(self.m.V, within = NonNegativeReals, bounds = (0,10))
        self.m.T = Var(self.m.X_pipe_T, within = NonNegativeReals, bounds = (278,408))
        self.m.e_node = Var(self.m.V, within = NonNegativeReals, bounds = (0.1,0.55))
        self.m.e_edge = Var(self.m.X, within = NonNegativeReals, bounds = (0.1,0.55))        
        self.m.beta = Var(self.m.no_fixed_vel, within = NonNegativeReals, bounds = (0,20))
        self.m.gamma = Var(self.m.no_fixed_vel, within = NonNegativeReals, bounds = (0,20))
        self.m.Pp = Var(within = NonNegativeReals)
        self.m.Pw = Var(within = NonNegativeReals)
        self.m.Pg = Var(within = NonNegativeReals)

        # Energy scaling factor.
        self.energy_scaling = 1e9

        ####Initialize Objective###
        def payment(m):
            return m.Cw*m.hours_in_day*(m.Pw) + m.Cg*m.hours_in_day*(m.Pg) + m.Cp*m.hours_in_day*(m.Pp)
        self.m.obj = Objective(rule=payment)

        ###Pipe constraints###
        
        #Momentum conservation
        def momentum_rule(m,u,v):
            a = (u,v)
            abs_q = -m.q[a]*(a in m.neg_vel) + m.q[a]*(a in m.pos_vel) + abs(m.q[a])*(a not in (m.pos_vel | m.neg_vel))
            return 0 == (m.p_node[v] - m.p_node[u])*1e5/m.L[a] + m.lamb[a]/(2*m.D[a]*m.rho[a]*m.Area[a]*m.Area[a])*m.q[a]*abs_q + m.g*m.rho[a]*m.h[a]
        self.m.momentum = Constraint(self.m.Ap, rule = momentum_rule)
        
        #Internal energy conservation
        def internal_energy_rule(m,u,v,k):
            a = (u,v)
            abs_q = -m.q[a]*(a in m.neg_vel) + m.q[a]*(a in m.pos_vel) + abs(m.q[a])*(a not in (m.pos_vel | m.neg_vel))
            if k != value(m.M[a]) and m.l[a] == 1:
                return 0 == m.q[a]/(m.rho[a]*m.Area[a])*(m.e_edge[a,k+1] - m.e_edge[a,k])*self.energy_scaling/m.delta_x[a] - m.lamb[a]/(2*m.D[a]*(m.rho[a]**2)*(m.Area[a]**3))*m.q[a]*m.q[a]*abs_q + 4*m.k[a]/m.D[a]*(m.T[a,k+1] - m.Tw)
            elif k != value(m.M[a]) and m.l[a] == 2:
                return 0 == m.q[a]/(m.rho[a]*m.Area[a])*(m.e_edge[a,k+1] - m.e_edge[a,k])*self.energy_scaling/m.delta_x[a] + 4*m.k[a]/m.D[a]*(m.T[a,k+1] - m.Tw)
            elif k != value(m.M[a]) and m.l[a] == 3:
                return 0 == m.e_edge[a,k+1] - m.e_edge[a,k]
            elif k == value(m.M[a]):
                return Constraint.Skip
        self.m.internal_energy = Constraint(self.m.X_pipe, rule = internal_energy_rule)

        #Temp node state equation
        def temp_edge_state_rule(m,u,v,k):
            a = (u,v)            
            if k != value(m.M[a]):
                exp = (m.e_edge[a,k+1]+m.e_edge[a,k])/2
                return m.T[a,k+1] ==  59.2453*exp*exp + 220.536*exp + 1.93729 + 273
            else:
                return Constraint.Skip
        self.m.temp_edge_state = Constraint(self.m.X_pipe_T, rule = temp_edge_state_rule)
        
        #Mass balance equation
        def mass_balance_rule(m,u):
            return (sum(m.q[a[1],a[2]] for a in m.delta_in if a[0] == u)
                    - sum(m.q[a[1],a[2]] for a in m.delta_out if a[0] == u) == 0)
        self.m.mass_balance = Constraint(self.m.V, rule = mass_balance_rule)
            
        #Positive and negative velocity fixing
        def velocity_rule(m,u,v):
            a = (u,v)
            if a in m.pos_vel:
                return m.q[a] >= 0
            elif a in m.neg_vel:
                return -m.q[a] >= 0
        self.m.velocity = Constraint(self.m.pos_vel | self.m.neg_vel, rule = velocity_rule)

        #Thermal mixing complementarity conditions
        def thermal_massflow_rule(m,u,v,mc):
            a = (u,v)
            if mc == 1:
                return m.q[a] == m.beta[a] - m.gamma[a]
            elif mc == 2:
                return m.beta[a]*m.gamma[a] == 0
        self.m.thermal_massflow = Constraint(self.m.no_fixed_vel, self.m.two_constraints, rule = thermal_massflow_rule)

        #Thermal mixing first equation
        def thermal_mixing_1_rule(m,u):
            
            # Creating lhs and rhs
            lhs = 0
            rhs = 0 
            for a in m.A:
                if a[0] == u:
                    if a in m.neg_vel:
                        lhs  = lhs - m.q[a]
                        rhs  = rhs - m.q[a]*m.e_edge[a[0],a[1],0]
                    elif a not in m.pos_vel:
                        lhs = lhs + m.gamma[a]
                        rhs = rhs + m.gamma[a]*m.e_edge[a[0],a[1],0]
                elif a[1] == u:
                    if a in m.pos_vel:
                        lhs  = lhs + m.q[a]
                        rhs  = rhs + m.q[a]*m.e_edge[a[0],a[1],value(m.M[(a[0],a[1])])]
                    elif a not in m.neg_vel:
                        lhs = lhs + m.beta[a]
                        rhs = rhs + m.beta[a]*m.e_edge[a[0],a[1],value(m.M[(a[0],a[1])])]
            lhs = lhs*m.e_node[u]

            # Returning lhs = rhs
            return lhs == rhs 
        self.m.thermal_mixing_1 = Constraint(self.m.V, rule = thermal_mixing_1_rule)

        #Thermal mixing second and third equation
        def thermal_mixing_2_rule(m,u,v,mc):
            a = (u,v)
            if a in m.pos_vel:
                if mc == 1:
                    return m.e_node[u] == m.e_edge[a,0]
                elif mc == 2:
                    return Constraint.Skip
            elif a in m.neg_vel:
                if mc == 1:
                    return m.e_node[v] == m.e_edge[a,value(m.M[a])]
                elif mc == 2:
                    return Constraint.Skip
            else:
                if mc == 1:
                    return 0 == m.gamma[a]*(m.e_edge[a,value(m.M[a])] - m.e_node[v])
                elif mc == 2:
                    return 0 == m.beta[a]*(m.e_edge[a,0] - m.e_node[u])
        self.m.thermal_mixing_2 = Constraint(self.m.A, self.m.two_constraints, rule = thermal_mixing_2_rule)

        #Bounds due to physics
        def bounds_node_dependent_rule(m,u,mc):
            if mc == 1:
                return m.p_minus[u] <= m.p_node[u]
            elif mc == 2:
                return m.p_node[u] <= m.p_plus[u]
            elif mc == 3:
                return m.e_minus[u] <= m.e_node[u]
            elif mc == 4:
                return m.e_node[u] <= m.e_plus[u]
        self.m.bounds_node_dependent = Constraint(self.m.V, self.m.four_constraints, rule = bounds_node_dependent_rule)
        def bounds_node_independent_rule(m,mc):
            if mc == 1:
                if m.upper_bound_on_Pp:
                    return  m.Pp <= value(m.Pp_plus)
                else:
                    return Constraint.Skip
            elif mc == 2:
                return m.Pw <= value(m.Pw_plus)
            elif mc == 3:
                if m.upper_bound_on_Pg:
                    return  m.Pg <= value(m.Pg_plus)
                else:
                    return Constraint.Skip
        self.m.bounds_node_independent = Constraint(self.m.three_constraints, rule=bounds_node_independent_rule)

        ###Consumer constraints###
        def consumer_constraints_rule(m,c,d,mc):
            a = (c,d)
            if mc == 1:
                return m.Pc[a]*1e-6 == m.q[a]/m.rho[a]*(m.e_edge[a,0] - m.e_edge[a,value(m.M[a])])
            elif mc == 2:
                return m.ebf[a] <= m.e_edge[a,value(m.M[a])] + 0.002
            elif mc == 3:
                return m.ebf[a] >= m.e_edge[a,value(m.M[a])] - 0.002
            elif mc == 3:
                return m.e_edge[(a,0)] - m.eff[a] >= 0
            elif mc == 4:
                return m.p_node[c] - m.p_node[d] >= 0
            elif mc == 5:
                return m.q[a] >= 0
        self.m.consumer_constraints = Constraint(self.m.Ac, self.m.five_constraints, rule = consumer_constraints_rule)

        ###Depot Constraints###
        def depot_constraints_rule(m,c,d,mc):
            a = (c,d)
            if mc == 1:
                return m.ps - m.stagnation_pressure_epsilon  <= m.p_node[c]
            elif mc == 2:
                return m.p_node[c] <= m.ps + m.stagnation_pressure_epsilon
            elif mc == 3:
                return m.Pp/1e2 == m.q[a]/m.rho[a]*(m.p_node[d] - m.p_node[c])
            elif mc == 4:
                 return (m.Pw + m.Pg)*1e-6 == m.q[a]/m.rho[a]*(m.e_edge[a,value(m.M[a])] - m.e_edge[a,0])
            elif mc == 5:
                return m.q[a] >= 0
        self.m.depot_constraints = Constraint(self.m.Ad, self.m.five_constraints, rule = depot_constraints_rule)

    @staticmethod
    def T_to_e(T):
        T0 = 1.93729
        T1 = 220.536
        T2 = 59.2453
        return 0.5*(1/T2)*(-T1+(T1**2-4*T2*(T0-T))**0.5)

    @staticmethod
    def e_to_T(e):
        T0 = 1.93729 + 273
        T1 = 220.536
        T2 = 59.2453
        return T0 + e*T1 + e*e*T2
    
    def repetitive_setup(self, data_dict):
        
        ###Add Sets###
        X_list1 = []
        X_list2 = []
        X_list3 = []
        for a in self.network.edges:
            if self.network.edges[a]['is_pipe']:
                X_list1 = X_list1 + [(a,x) for x in range(0,self.network.edges[a]["M"])]
                X_list2 = X_list2 + [(a,x) for x in range(0,self.network.edges[a]["M"])]
                if self.network.edges[a]["level"] <= 2:
                    X_list3 = X_list3 + [(a,x) for x in range(1,self.network.edges[a]["M"])]
            else:
                X_list1.append((a,0))
                X_list1.append((a,self.network.edges[a]["M"]-1))
        data_dict["X"] = {None: X_list1}
        data_dict["X_pipe"] = {None: X_list2}
        data_dict["X_pipe_T"] = {None: X_list3}
        data_dict["X_pipe_bounds"] = {None: [(a,0) for a in self.network.edges if self.network.edges[a]['is_pipe']] + [(a,self.network.edges[a]["M"]-1) for a in self.network.edges if self.network.edges[a]['is_pipe']]}

        ###Add parameters##
        data_dict["l"] = {a:self.network.edges[a]['level'] for a in self.network.edges if self.network.edges[a]["is_pipe"]}
        data_dict["M"] = {a:self.network.edges[a]['M']-1 for a in self.network.edges}
        data_dict["delta_x"] = {a:self.network.edges[a]["length"]/(self.network.edges[a]['M']-1) for a in self.network.edges if self.network.edges[a]["is_pipe"]}

    def setup_intance(self):
        ###Conversion parameters and initialization###
        bar_to_Pa = 1e5
        C_to_K = 273
        data_dict = {}
        hours_in_day = 24        

        ####OPTIMIZATION PROBLEM####
        ###Add sets###
        data_dict["Ap"] = {None:[a for a in self.network.edges if self.network.edges[a]['is_pipe']]}
        data_dict["Ac"] = {None:[a for a in self.network.edges if self.network.edges[a]['is_consumer']]}
        data_dict["Ad"] = {None:[a for a in self.network.edges if self.network.edges[a]['is_source']]}
        data_dict["V"] = {None:list(self.network.nodes)}
        data_dict["delta_out"] = {None:[(u,a) for u in self.network.nodes for a in self.network.out_edges(u)]}
        data_dict["delta_in"] = {None:[(u,a) for u in self.network.nodes for a in self.network.in_edges(u)]}
        data_dict["pos_vel"] = {None:[a for a in self.network.edges if self.network.edges[a]["flow_is_fixed_pos"]]}
        data_dict["neg_vel"] = {None:[a for a in self.network.edges if self.network.edges[a]["flow_is_fixed_neg"]]}
        self.repetitive_setup(data_dict)

        ###Add parameters###
        data_dict["L"] = {a:self.network.edges[a]["length"] for a in self.network.edges if self.network.edges[a]["is_pipe"]}        
        data_dict["D"] = {a:self.network.edges[a]["diameter"] for a in self.network.edges if self.network.edges[a]["is_pipe"]}
        data_dict["Area"] = {a:math.pi*(self.network.edges[a]["diameter"]/2)*(self.network.edges[a]["diameter"]) for a in self.network.edges if self.network.edges[a]["is_pipe"]}
        data_dict["h"] = {a:self.network.edges[a]["slope"] for a in self.network.edges if self.network.edges[a]["is_pipe"]}
        #print(sum([abs(self.network.edges[a]["consumer_factor"]/hours_in_day) for a in self.network.edges if self.network.edges[a]["is_consumer"]]))
        data_dict["Pc"] = {a: abs(self.network.edges[a]["consumer_factor"]/hours_in_day) for a in self.network.edges if self.network.edges[a]["is_consumer"]}
        data_dict["k"] = {a:self.network.edges[a]["ht_coefficient"] for a in self.network.edges if self.network.edges[a]["is_pipe"]}
        data_dict["eff"] = {a:self.T_to_e(self.network.edges[a]["temperature"]+15) for a in self.network.edges if self.network.edges[a]["is_consumer"]}
        eff_list = [self.T_to_e(self.network.edges[a]["temperature"]+15) for a in self.network.edges if self.network.edges[a]["is_consumer"]]
        data_dict["ebf"] = {a:self.T_to_e(self.network.edges[a]["temperature"]) for a in self.network.edges if self.network.edges[a]["is_consumer"]}
        ebf_list = [self.T_to_e(self.network.edges[a]["temperature"]) for a in self.network.edges if self.network.edges[a]["is_consumer"]]
        def mean(l):
            return sum(l)/len(l)
        self.eff_mean = mean(eff_list)
        self.ebf_mean = mean(ebf_list)
        data_dict["Tw"] = {None:self.parameters["temp_ground"]+C_to_K}
        data_dict["ps"] = {None:self.parameters["stagnation_pressure"]}
        data_dict["g"] = {None:self.parameters["gravity_coefficient"]}
        data_dict["Cw"] = {None:self.parameters["cost_waste_coef"]}
        data_dict["Cg"] = {None:self.parameters["cost_gas_coef"]}
        data_dict["Cp"] = {None:self.parameters["cost_pressure_coef"]}
        data_dict["lamb"] = {a:math.pow(2*math.log10(self.network.edges[a]["diameter"]/self.network.edges[a]["roughness"])+1.138,-2) for a in self.network.edges if self.network.edges[a]["is_pipe"]}
        data_dict["p_plus"] = {u:self.parameters["p_plus"] for u in self.network.nodes}
        data_dict["p_minus"] = {u:self.parameters["p_minus"] for u in self.network.nodes}
        data_dict["e_plus"] = {u:self.T_to_e(self.parameters["temp_plus"]) for u in self.network.nodes}
        data_dict["e_minus"] = {u:self.T_to_e(self.parameters["temp_ground"]) for u in self.network.nodes}
        data_dict["upper_bound_on_Pg"] = {None:self.parameters["upper_bound_on_Pg"] == 1}
        data_dict["upper_bound_on_Pp"] = {None:self.parameters["upper_bound_on_Pp"] == 1}
        data_dict["Pw_plus"] = {None:self.parameters["power_w_plus"]}
        data_dict["Pg_plus"] = {None:self.parameters["power_g_plus"]}
        data_dict["Pp_plus"] = {None:self.parameters["power_p_plus"]}
        data_dict["rho"] = {a:self.parameters["rho"] for a in self.network.edges}
        data_dict["consumer_outlet_temperature_epsilon"] = {None:self.parameters["consumer_outlet_temperature_epsilon"]}
        data_dict["stagnation_pressure_epsilon"] = {None:self.parameters["stagnation_pressure_epsilon"]}
        data_dict["hours_in_day"] = {None: self.parameters["hours_in_day"]}
        
        ###Initialize creation###
        self.state_computation_data = {None:data_dict}
        self.instance = self.m.create_instance(self.state_computation_data)
        #self.instance.pprint()

    def old_initial_warmstart(self):
        ###WarmStart values of all parameters###
        self.instance.Pw.value = self.instance.Pw_plus.value
        self.instance.Pg.value = sum(self.instance.Pc[a] for a in self.instance.Ac) - self.instance.Pw_plus.value
        self.instance.Pp.value = 0.5
        for a in self.instance.A:
            self.instance.q[a].value = 2
            if a in self.instance.Ap and a not in (self.instance.pos_vel | self.instance.neg_vel):
                self.instance.beta[a].value = 2
                self.instance.gamma[a].value = 0
            elif a in self.instance.Ac:
                self.instance.e_edge[a,0].value = self.instance.eff[a]
                self.instance.e_edge[a,value(self.instance.M[a])].value = self.instance.ebf[a]
                self.instance.e_node[a[0]].value = self.instance.eff[a]
                self.instance.e_node[a[1]].value = self.instance.ebf[a]
        for u in self.instance.V:
            self.instance.p_node[u].value = self.instance.ps.value
                
        for a in self.instance.A:
            if a not in self.instance.Ac:
                if self.network.nodes[a[0]]["in_foreflow"]:
                    if self.instance.e_node[a[0]].value == None:
                        self.instance.e_node[a[0]].value = self.eff_mean
                    if self.instance.e_node[a[1]].value == None:
                        self.instance.e_node[a[1]].value = self.eff_mean
                elif self.network.nodes[a[0]]["in_backflow"]:
                    if self.instance.e_node[a[0]].value == None:
                        self.instance.e_node[a[0]].value = self.ebf_mean
                    if self.instance.e_node[a[1]].value == None:
                        self.instance.e_node[a[1]].value = self.ebf_mean
            if a in self.instance.Ap:
                m = (self.instance.e_node[a[1]].value - self.instance.e_node[a[0]].value)/value(self.instance.M[a])
                p = self.instance.e_node[a[0]].value
                if self.network.edges[a]["level"] <= 2:
                    for x in range(0,value(self.instance.M[a])):
                        self.instance.e_edge[a,x].value = m*x+p
                        if (x != 0):
                            self.instance.T[a,x].value = self.e_to_T(m*x+p)
            elif self.instance.Ad:
                self.instance.e_edge[a,0].value = self.instance.e_node[a[0]].value
                self.instance.e_edge[a,value(self.instance.M[a])].value = self.instance.e_node[a[1]].value


    def initial_energy_warmstart(self):

        # First we fix e and p for all nodes
        for u in self.instance.V:
            if self.network.nodes[u]["in_foreflow"]:
                self.instance.e_node[u].value = self.T_to_e(self.parameters["temp_plus"])
            else:
                self.instance.e_node[u].value = self.ebf_mean

        # Then we fix e and T for all pipes:
        for a in self.instance.Ap:
            if self.network.nodes[a[0]]["in_foreflow"]:
                for i in range(self.network.edges[a]["M"]):
                    self.instance.e_edge[a,i].value = self.T_to_e(self.parameters["temp_plus"])
                    if self.network.edges[a]["level"] <= 2 and i != 0:
                        self.instance.T[a,i].value = self.parameters["temp_plus"]
            elif self.network.nodes[a[0]]["in_backflow"]:
                for i in range(self.network.edges[a]["M"]):
                    self.instance.e_edge[a,i].value = self.ebf_mean
                    if self.network.edges[a]["level"] <= 2 and i != 0:
                        self.instance.T[a,i].value = self.T_to_e(self.ebf_mean)

        # Then we fix e and T for all consumers:
        for a in self.instance.Ac:
            self.instance.e_edge[a,0].value = self.T_to_e(self.parameters["temp_plus"])
            self.instance.e_edge[a,value(self.instance.M[a])].value = self.ebf_mean

        # Then we fix e and T for the depot:
        for a in self.instance.Ad:
            self.instance.e_edge[a,0].value = self.ebf_mean
            self.instance.e_edge[a,value(self.instance.M[a])].value = self.T_to_e(self.parameters["temp_plus"]) 

    def initial_power_warmstart(self):
        # Fixing the power variables
        sum_of_consumers_power = sum(self.instance.Pc[a] for a in self.instance.Ac)
        self.instance.Pw.value = min(sum_of_consumers_power,self.instance.Pw_plus.value)
        self.instance.Pg.value = sum_of_consumers_power - self.instance.Pw.value
        #print(self.instance.Pg.value)

    def initial_massflow_warmstart(self):

        #Computing the massflow due to the temperature difference for depot and consumer
        for a in self.instance.Ad:
            self.instance.q[a].value = (self.instance.Pw.value + self.instance.Pg.value)*1e-6*value(self.instance.rho[a])/(self.instance.e_node[a[1]].value - self.instance.e_node[a[0]].value)
        for a in self.instance.Ac:
            self.instance.q[a].value = value(self.instance.Pc[a])*1e-6*value(self.instance.rho[a])/(self.instance.e_node[a[0]].value - self.instance.e_node[a[1]].value)

        #Creating the linear system for computing the residual massflows for the pipes 
        graph_withouth_consumers_depot = self.network.get_graph_without_consumers_depot()
        edge_list = [a for a in self.instance.Ap]
        #print(edge_list)
        node_list_ff = [u for u in self.instance.V if self.network.nodes[u]['in_foreflow']]
        node_list_bf = [v for v in self.instance.V if self.network.nodes[v]['in_backflow']]
        node_list = node_list_ff + node_list_bf
        #print(node_list)
        A = incidence_matrix(graph_withouth_consumers_depot, nodelist = node_list, edgelist = edge_list, oriented = True).toarray()
        Apinv = np.linalg.pinv(A)
        b = [0 for i in range(len(node_list))]
        for a in self.instance.Ac:
            index = 0
            for u in node_list_ff:
                if a[0] == u:
                    b[index] = self.instance.q[a].value
                index = index + 1
            for u in node_list_bf:
                if a[1] == u:
                    b[index] = - self.instance.q[a].value
                index = index + 1
        for a in self.instance.Ad:
            index = 0
            for u in node_list_ff:
                if a[1] == u:
                    b[index] = - self.instance.q[a].value
                index = index + 1
            for u in node_list_bf:
                if a[0] == u:
                    b[index] = self.instance.q[a].value
                index = index + 1
        b = np.array(b)
        

        # Solving the linear system and extracting the solution
        index = 0
        q_vector = Apinv.dot(b)
        #print(q_vector)
        for a in self.instance.Ap:
            self.instance.q[a].value = q_vector[index]
            index = index + 1

    def initial_mixing_warmstart(self):
        for a in self.instance.Ap - self.instance.pos_vel - self.instance.neg_vel:
            if self.instance.q[a].value > 0:
                self.instance.beta[a].value = self.instance.q[a].value
                self.instance.gamma[a].value = 0
            elif self.instance.q[a].value < 0:
                self.instance.beta[a].value = 0
                self.instance.gamma[a].value = self.instance.q[a].value

    def initial_pressure_warmstart(self):
        
        # Creating the linear system for computing the residual pressures 
        graph_withouth_consumers_depot = self.network.get_graph_without_consumers_depot()
        edge_list = [a for a in self.instance.Ap]
        node_list_ff = [u for u in self.instance.V if self.network.nodes[u]['in_foreflow']]
        node_list_bf = [v for v in self.instance.V if self.network.nodes[v]['in_backflow']]
        node_list = node_list_ff + node_list_bf
        A = incidence_matrix(graph_withouth_consumers_depot, nodelist = node_list, edgelist = edge_list, oriented = True).toarray().T
        Apinv = np.linalg.pinv(A)

        # Create node index mapping
        mapping = {}
        index = 0
        for node in node_list:
            mapping[node] = index
            index = index + 1

        # Creating b vector
        b = np.zeros((len(edge_list),))
        index = 0
        for a in edge_list:
            
            # Extract parameters
            L = value(self.instance.L[a])
            lamb = value(self.instance.lamb[a])
            D = value(self.instance.D[a])
            h = value(self.instance.h[a])
            rho = value(self.instance.rho[a])
            g = value(self.instance.g)
            v = self.instance.q[a].value/(self.instance.Area[a]*rho)

            # Filling list with adequate parameter
            cst1 = -L*lamb*rho*abs(v)*v/(2*D*1e5)
            cst2 = -L*g*rho*h/(1e5)
            b[index] = cst1+cst2
            index = index + 1

        # Solving the linear system and extracting the solution
        p_vector = Apinv.dot(b)
            
        # Computing and adding the bf offset
        for a in self.instance.Ad:
            offset = value(self.instance.ps) - p_vector[mapping[a[0]]]
        p_vector[len(node_list_ff):] += offset

        # Computing and adding the ff offset
        offset = 0
        for a in self.instance.Ac:
            difference = p_vector[mapping[a[1]]] - p_vector[mapping[a[0]]]
            offset = max(offset,difference)
        for a in self.instance.Ad:
            difference = p_vector[mapping[a[0]]] - p_vector[mapping[a[1]]]
            offset = max(offset,difference)
        p_vector[:len(node_list_ff)] += value(offset)

        # print(offset)
        # print(node_list)
        # print(p_vector)
        
        # Extracting solution as warmstart
        index = 0
        for node in node_list:
            self.instance.p_node[node].value = p_vector[index]
            index = index + 1

        # Extracting power for depot
        for a in self.instance.Ad:
            q = self.instance.q[a].value
            rho = value(self.instance.rho[a])
            p_out = self.instance.p_node[a[1]].value
            p_in = self.instance.p_node[a[0]].value            
            difference = q*(p_out-p_in)*1e2/rho
        self.instance.Pp.value = difference
        #print(difference)
            

    def initial_warmstart(self):
        self.initial_energy_warmstart()
        self.initial_power_warmstart()
        self.initial_massflow_warmstart()
        self.initial_pressure_warmstart()
        self.initial_mixing_warmstart()
    
    def solve(self):
        
        ###Solve Model###
        self.opt = SolverFactory('gams')
        if self.computational_parameters["solver"] == "knitro":
           self.results = self.opt.solve(self.instance, solver=self.computational_parameters["solver"], tee=True, add_options=['option reslim=2000;','GAMS_MODEL.optfile = 1;','$onecho > knitro.opt', 'maxit 1000000', '$offecho', 'option optcr=0.001;'], warmstart=True, keepfiles=True)
        else:
            self.results = self.opt.solve(self.instance, solver=self.computational_parameters["solver"], tee=True, add_options=['option reslim=2000;', 'option optcr=0.001;'], warmstart=True, keepfiles=True)

        # Save computational time
        self.nlp_solving_time.append(getattr(self.results["Solver"],'User time'))
            
        # print(self.instance.Pw.value)
        # print(self.instance.Pp.value)
        # print(self.instance.Pg.value)

    def get_nlp_solving_time(self):
        return self.nlp_solving_time

    def reconstruct(self):
        ###Setup new data###
        data_dict = self.state_computation_data[None]
        self.repetitive_setup(data_dict)
        
        ###Create Model and Save Old Results###
        old_instance = copy.deepcopy(self.instance)
        start = time.time()
        self.instance = self.m.create_instance(self.state_computation_data)
        

        ###Warmstarting###
        ##Basic variables##
        self.instance.Pp.value = old_instance.Pp.value
        self.instance.Pw.value = old_instance.Pw.value
        self.instance.Pg.value = old_instance.Pg.value

        ##Nodal stuff##
        for u in self.instance.V:
            self.instance.p_node[u].value = old_instance.p_node[u].value
            self.instance.e_node[u].value = old_instance.e_node[u].value

        ##Edge stuff
        for a in self.instance.A:
            #Edge Specific#
            self.instance.q[a].value = old_instance.q[a].value
            if self.network.edges[a]["is_pipe"] and (a not in self.instance.pos_vel) and (a not in self.instance.neg_vel):
                self.instance.beta[a].value = old_instance.beta[a].value
                self.instance.gamma[a].value = old_instance.gamma[a].value
            #Discretization specific#
            M_frac = (value(self.instance.M[a]))/(value(old_instance.M[a]))
            for k in range(0,value(self.instance.M[a]+1)):
                if ((M_frac) >= 1 and ((k/M_frac) % 1) == 0) or (M_frac) < 1:
                    self.instance.e_edge[a,k].value = old_instance.e_edge[a,(k/M_frac)].value
                    if self.network.edges[a]["is_pipe"] and self.network.edges[a]["level"] <= 2 and k != 0:
                        self.instance.T[a,k].value = self.e_to_T((self.instance.e_edge[a,k].value + self.instance.e_edge[a,k-1].value)/2)
                else:
                    self.instance.e_edge[a,k].value =(old_instance.e_edge[a,math.ceil(k/M_frac)].value*((k/M_frac)-math.floor(k/M_frac))
                                                      + old_instance.e_edge[a,math.floor(k/M_frac)].value*(math.ceil(k/M_frac)-(k/M_frac)))/(math.ceil(k/M_frac)-math.floor(k/M_frac))
                    if self.network.edges[a]["is_pipe"] and self.network.edges[a]["level"] <= 2 and k != 0:
                        self.instance.T[a,k].value = self.e_to_T((self.instance.e_edge[a,k].value + self.instance.e_edge[a,k-1].value)/2)
                        
#        print("Time is:" + str(time.time()-start))
                

    def get_internal_energy_from_solution(self):
        return {(a,k):(self.instance.e_edge[a,k].value*self.energy_scaling) for a in self.instance.A for k in range(0,value(self.instance.M[a]+1)) if self.network.edges[a]["is_pipe"]}

    def get_velocity_from_solution(self):
        return {a:self.instance.q[a].value/value(self.instance.Area[a]*self.instance.rho[a]) for a in self.instance.A if self.network.edges[a]["is_pipe"]}

    @staticmethod
    def json_import(file_name):
        with open(file_name) as json_file:
            data = json.load(json_file)
        return data

    @staticmethod
    def table_to_csv(table, file_name, nl, dl):
        with open(file_name, 'w', newline=nl) as csvfile:
            csvwriter = csv.writer(csvfile, delimiter=dl, quoting=csv.QUOTE_NONE)
            for l in table:
                csvwriter.writerow(l)
    
    def save_results(self, file_name_nodes, file_name_edges, final_output):
        node_presets = self.json_import("/home/marius/Documents/trier/dhn/dhn-adaptive/code/src/data_extraction/node_presets.json")
        edge_presets = self.json_import("/home/marius/Documents/trier/dhn/dhn-adaptive/code/src/data_extraction/edge_presets.json")
        node_data = [node_presets[n].split('/') + [str(round(self.instance.p_node[n].value,2)) , str(round(self.e_to_T(self.instance.e_node[n].value),2))+","] for n in self.network.nodes if self.network.nodes[n]["in_foreflow"]]
        node_data[len(node_data)-1][len(node_data[len(node_data)-1])-1] = node_data[len(node_data)-1][len(node_data[len(node_data)-1])-1][0:-1]+'}'
        edge_data2 = [edge_presets[str(e)].split('/') + [str(round(self.instance.q[e].value,2)), "solid"+ ","] for e in self.network.edges if self.network.nodes[e[0]]["in_foreflow"]  if self.network.nodes[e[1]]["in_foreflow"]]
        edge_data2[len(edge_data2)-1][len(edge_data2[len(edge_data2)-1])-1] = edge_data2[len(edge_data2)-1][len(edge_data2[len(edge_data2)-1])-1][0:-1]+'}'
        edge_data = edge_data2
        self.table_to_csv(node_data, file_name_nodes, '\n', '/')
        self.table_to_csv(edge_data, file_name_edges, '\n', '/')
        filenames = ["/home/marius/Documents/trier/dhn/dhn-adaptive/code/src/data_extraction/header.tikz", "/home/marius/Documents/trier/dhn/dhn-adaptive/code/src/data_extraction/node_sol.tikz", "/home/marius/Documents/trier/dhn/dhn-adaptive/code/src/data_extraction/interm.tikz","/home/marius/Documents/trier/dhn/dhn-adaptive/code/src/data_extraction/edge_sol.tikz","/home/marius/Documents/trier/dhn/dhn-adaptive/code/src/data_extraction/closer.tikz"]
        with open(final_output, 'w') as outfile:
            for fname in filenames:
                with open(fname) as infile:
                    for line in infile:
                        outfile.write(line)
        
    def plot_all_results(self,with_colors = True):
        def mean(l):
            return sum(l)/len(l)
        node_size,no_height_given,max_diff,factor,text_size,ax,fig = self.network.plot_network()
        offset = factor*max_diff/10
        q_values = [self.instance.q[a].value for a in self.instance.A]
        q_val_pipes = {a:self.instance.q[a].value for a in self.instance.A}
        max_q_value = max(q_values)
        min_q_value = min(q_values)
        max_width = 4
        min_width = 2
        percentage_offset = 0.10
        length_offset = 100
        try:
            a = (max_width - min_width)/(max_q_value - min_q_value)
        except ZeroDivisionError:
            a = (max_width - min_width)/(max_q_value - min_q_value + 1)
        b = max_width-a*max_q_value
        x_neighbors = []
        y_neighbors = []
        z_neighbors = []
        for n in self.instance.V:
            x = self.network.nodes[n]["position"][0]
            y = self.network.nodes[n]["position"][1]
            z = self.network.nodes[n]["position"][2]-max_diff*factor*no_height_given*self.network.nodes[n]["in_backflow"]
            ax.text(x,y,z - offset*self.network.nodes[n]["in_backflow"] + offset*self.network.nodes[n]["in_foreflow"],'p = '+str(round(self.instance.p_node[n].value,3))+'\n'+'e = '+str(round(self.instance.e_node[n].value*1e0,3)) + '\n'+'T = '+str(round(self.e_to_T(self.instance.e_node[n].value),3)), ha='center', va= 'top'*self.network.nodes[n]["in_backflow"]+'bottom'*self.network.nodes[n]["in_foreflow"], size = text_size)
        for n1,n2 in self.instance.A:
            x_for_lines = [self.network.nodes[n1]["position"][0],self.network.nodes[n2]["position"][0]]
            y_for_lines = [self.network.nodes[n1]["position"][1],self.network.nodes[n2]["position"][1]]
            z_for_lines = [self.network.nodes[n1]["position"][2]-max_diff*factor*no_height_given*self.network.nodes[n1]["in_backflow"],self.network.nodes[n2]["position"][2]-max_diff*factor*no_height_given*self.network.nodes[n2]["in_backflow"]]
            width_of_line = a*q_val_pipes[(n1,n2)] + b
            ax.plot(x_for_lines,y_for_lines,z_for_lines,color=('red'*self.network.edges[(n1,n2)]['is_source']+'green'*self.network.edges[(n1,n2)]['is_consumer']+'blue'*self.network.edges[(n1,n2)]['is_pipe'])*with_colors+'blue'*(not with_colors), linewidth=width_of_line)
            ax.text(mean(x_for_lines) - 220*offset*(self.network.edges[(n1,n2)]["is_consumer"] or self.network.edges[(n1,n2)]["is_source"]),mean(y_for_lines),(mean(z_for_lines) - offset*self.network.edges[(n1,n2)]["is_pipe"]*self.network.nodes[n1]["in_foreflow"]+offset*self.network.edges[(n1,n2)]["is_pipe"]*self.network.nodes[n1]["in_backflow"]),'q = '+str(round(q_val_pipes[(n1,n2)],3)),ha='center', va= 'bottom'*self.network.nodes[n1]["in_backflow"]*self.network.edges[(n1,n2)]["is_pipe"]+'top'*self.network.nodes[n1]["in_foreflow"]*self.network.edges[(n1,n2)]["is_pipe"]+'center'*(self.network.edges[(n1,n2)]["is_consumer"] or self.network.edges[(n1,n2)]["is_source"]), size = text_size)
            if self.network.edges[(n1,n2)]["is_pipe"]:
                for i in range(1,value(self.instance.M[(n1,n2)])+2):
                    percentage_offset = 1/(self.instance.M[(n1,n2)]+2)*i
                    x_pos = self.network.nodes[n1]["position"][0]*(1-percentage_offset) + self.network.nodes[n2]["position"][0]*percentage_offset
                    y_pos = self.network.nodes[n1]["position"][1]*(1-percentage_offset) + self.network.nodes[n2]["position"][1]*percentage_offset
                    z_pos = (self.network.nodes[n1]["position"][2]-max_diff*factor*no_height_given*self.network.nodes[n1]["in_backflow"])*(1-percentage_offset) + (self.network.nodes[n2]["position"][2] -max_diff*factor*no_height_given*self.network.nodes[n2]["in_backflow"])*percentage_offset
                    x_neighbors.append(x_pos)
                    y_neighbors.append(y_pos)
                    z_neighbors.append(z_pos)
                    ax.text(x_pos,y_pos,z_pos  - offset*self.network.nodes[n1]["in_backflow"] + offset*self.network.nodes[n1]["in_foreflow"],'e = '+str(round(self.instance.e_edge[n1,n2,i-1].value*1e0,3)) + '\n'+'T = '+str(round(self.e_to_T(self.instance.e_edge[n1,n2,i-1].value),3)), color='black', ha='center', va='center', size=text_size)
            if not self.network.edges[(n1,n2)]["is_pipe"]:
                for i in range(1,value(self.instance.M[(n1,n2)])+2):
                    percentage_offset = 1/(self.instance.M[(n1,n2)]+2)*i
                    x_pos = self.network.nodes[n1]["position"][0]*(1-percentage_offset) + self.network.nodes[n2]["position"][0]*percentage_offset
                    y_pos = self.network.nodes[n1]["position"][1]*(1-percentage_offset) + self.network.nodes[n2]["position"][1]*percentage_offset
                    z_pos = (self.network.nodes[n1]["position"][2]-max_diff*factor*no_height_given*self.network.nodes[n1]["in_backflow"])*(1-percentage_offset) + (self.network.nodes[n2]["position"][2] -max_diff*factor*no_height_given*self.network.nodes[n2]["in_backflow"])*percentage_offset
                    x_neighbors.append(x_pos)
                    y_neighbors.append(y_pos)
                    z_neighbors.append(z_pos)
                    ax.text(x_pos - 220*offset, y_pos, z_pos,'e = '+str(round(self.instance.e_edge[n1,n2,i-1].value*1e0,3)) + '\n'+'T = '+str(round(self.e_to_T(self.instance.e_edge[n1,n2,i-1].value),3)), color='black', ha='center', va='center', size=text_size)
        ax.scatter(x_neighbors,y_neighbors,z_neighbors, s=node_size/4, color='black', alpha = 1)
        ax.grid(False)
        ax.axis('off')
        fig.show()

    def plot_node_results(self,with_colors = True,plot_network_file=None):
        def mean(l):
            return sum(l)/len(l)
        node_size,no_height_given,max_diff,factor,text_size,ax,fig = self.network.plot_network()
        offset = factor*max_diff/10
        q_values = [self.instance.q[a].value for a in self.instance.A]
        q_val_pipes = {a:self.instance.q[a].value for a in self.instance.A}
        max_q_value = max(q_values)
        min_q_value = min(q_values)
        max_width = 4
        min_width = 2
        percentage_offset = 0.10
        length_offset = 100
        try:
            a = (max_width - min_width)/(max_q_value - min_q_value)
        except ZeroDivisionError:
            a = (max_width - min_width)/(max_q_value - min_q_value + 1)
        b = max_width-a*max_q_value
        x_neighbors = []
        y_neighbors = []
        z_neighbors = []
        for n in self.instance.V:
            x = self.network.nodes[n]["position"][0]
            y = self.network.nodes[n]["position"][1]
            z = self.network.nodes[n]["position"][2]-max_diff*factor*no_height_given*self.network.nodes[n]["in_backflow"]
            ax.text(x,y,z - offset*self.network.nodes[n]["in_backflow"] + offset*self.network.nodes[n]["in_foreflow"],'p = '+str(round(self.instance.p_node[n].value,3))+'\n'+'e = '+str(round(self.instance.e_node[n].value*1e0,3)) + '\n'+'T = '+str(round(self.e_to_T(self.instance.e_node[n].value),3)), ha='center', va= 'top'*self.network.nodes[n]["in_backflow"]+'bottom'*self.network.nodes[n]["in_foreflow"], size = text_size)
        for n1,n2 in self.instance.A:
            x_for_lines = [self.network.nodes[n1]["position"][0],self.network.nodes[n2]["position"][0]]
            y_for_lines = [self.network.nodes[n1]["position"][1],self.network.nodes[n2]["position"][1]]
            z_for_lines = [self.network.nodes[n1]["position"][2]-max_diff*factor*no_height_given*self.network.nodes[n1]["in_backflow"],self.network.nodes[n2]["position"][2]-max_diff*factor*no_height_given*self.network.nodes[n2]["in_backflow"]]
            width_of_line = a*q_val_pipes[(n1,n2)] + b
            ax.plot(x_for_lines,y_for_lines,z_for_lines,color=('red'*self.network.edges[(n1,n2)]['is_source']+'green'*self.network.edges[(n1,n2)]['is_consumer']+'blue'*self.network.edges[(n1,n2)]['is_pipe'])*with_colors+'blue'*(not with_colors), linewidth=width_of_line)
            ax.text(mean(x_for_lines) - 220*offset*(self.network.edges[(n1,n2)]["is_consumer"] or self.network.edges[(n1,n2)]["is_source"]),mean(y_for_lines),(mean(z_for_lines) - offset*self.network.edges[(n1,n2)]["is_pipe"]*self.network.nodes[n1]["in_foreflow"]+offset*self.network.edges[(n1,n2)]["is_pipe"]*self.network.nodes[n1]["in_backflow"]),'q = '+str(round(q_val_pipes[(n1,n2)],3)),ha='center', va= 'bottom'*self.network.nodes[n1]["in_backflow"]*self.network.edges[(n1,n2)]["is_pipe"]+'top'*self.network.nodes[n1]["in_foreflow"]*self.network.edges[(n1,n2)]["is_pipe"]+'center'*(self.network.edges[(n1,n2)]["is_consumer"] or self.network.edges[(n1,n2)]["is_source"]), size = text_size)
        ax.grid(False)
        ax.axis('off')
        # Saving or displaying
        if plot_network_file != None:
            fig.savefig(plot_network_file + "_network_visu.pgf", bbox_inches='tight')
        else:
            fig.show()

        
