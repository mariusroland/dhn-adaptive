import numpy as np
import copy
from dhn import *
from error_measure import *
from error_estimate import *
from exact_error import *

class switching_strategy():
	
	def __init__(self, network = None, mode = None, computational_parameters=None, error_measure_instance=None):
		
		self.pipes = None # List of pipe id (2-tuples)
		self.pipe_data = None # List of pipe data (3-tuples)
		self.candidates = None # List of pipe candidates for switching (4-tuple)
		self.error_measure_instance = None # Instance of provided error measure class
		self.max_level = 3 # Maxium number of pipe model levels

		self.refinement_rule = lambda n: 2*n - 1
		self.coarsening_rule = lambda n: int(0.5*(n - 1) + 1)

		# Default flag values (do not change).
		self.default_switching_mode_up = "up"
		self.default_switching_mode_down = "down"
		self.default_switching_mode_refine = "refine"
		self.default_switching_mode_coarsen = "coarsen"

		# Flags.
		self.mode = None # Indicating current switching mode: up, down, refine, coarsen
		self.error_measure_type = None # Flag (string) indicating which error measure is provided
		self.visited_switching_mode = {self.default_switching_mode_up:False, self.default_switching_mode_down:False,
					       self.default_switching_mode_refine:False, self.default_switching_mode_coarsen:False} #Indicates if specific mode has been visisted for saving the new data later
		
		self.print_model_grid_changes = False # outputs the change of model levels and grids for each pipe

		# Computational information
		self.sets_sizes = {self.default_switching_mode_up:[], self.default_switching_mode_down:[],
				   self.default_switching_mode_refine:[], self.default_switching_mode_coarsen:[]}
		
		# Set network data.
		if network is not None:
			self.set_network(network)
		
		# Set switching mode.
		if mode is not None:
			self.set_switching_mode(mode)
			
		# Set computational parameters.
		if computational_parameters is not None:
			self.set_computational_parameters(computational_parameters)

		# Set error measure class.
		if error_measure_instance is not None:
			self.set_error_measure(error_measure_instance)
		
	def compute_model_upswitching_set(self):
		
		self.set_switching_mode('up')
		
		# Compute up-switching pipe candidates, error deltas and new model levels.
		self.compute_model_level_switching_candidates()

		# Compute up-switching set U.
		self.compute_switching_set()
		
	def compute_model_downswitching_set(self):

		self.set_switching_mode('down')

		# Compute down-switching pipe candidates, error deltas and new model levels.
		self.compute_model_level_switching_candidates()

		# Compute down-switching set D.
		self.compute_switching_set()
		
	def compute_grid_refinement_set(self):
		
		self.set_switching_mode('refine')

		# Compute discretization error.
		self.compute_discretization_error()

		# Compute refinement set R.
		self.compute_switching_set()

	def compute_grid_coarsening_set(self):
		
		self.set_switching_mode('coarsen')

		# Compute discretization error.
		self.compute_discretization_error()

		# Compute coarsening set C.
		self.compute_switching_set()

	def compute_model_level_switching_candidates(self):

		# Compute set A_{pi}^{>\eps} or A_{pi}^{<\eps} as a tuple of size 4 for each pipe:
		# 	(pipe, in_candidates, delta_eta_m, new_level)
		#	in_candidates: boolean to indicate if pipe is in the set of up/down-switching candidates
		#	delta_eta_m: model error difference
		#	new_level: new pipe model level
		# (This is done in order to store all computed information.)

		self.candidates = []

		# Iterate over each pipe to find candidates, new model levels and error deltas.
		for pipe in self.pipes:

			# # Skip any pipe that only has 2 gridpoints.
			# if self.get_no_gridpoints(pipe) == 2:
			# 	in_candidates = False
			# 	delta_eta_m = None
			# 	new_level = None
			# 	self.candidates.append((pipe, in_candidates, delta_eta_m, new_level))
			# 	continue
			
			# Indicates if pipe is in set A_{pi}^{>\eps} or A_{pi}^{<\eps}.
			
			level = self.get_model_level(pipe)
			if level == 3 and self.mode == self.default_switching_mode_down:
				in_candidates = False
				delta_eta_m = None
				new_level = 3
				self.candidates.append((pipe, in_candidates, delta_eta_m, new_level))
				continue
			if level == 1 and self.mode == self.default_switching_mode_up:
				in_candidates = False
				delta_eta_m = None
				new_level = 1
				self.candidates.append((pipe, in_candidates, delta_eta_m, new_level))
				continue
			
			in_candidates = False
			level = self.get_model_level(pipe)

			# Get model error.
			eta_m = self.error_measure_instance.get_model_error(pipe, level)

			# Determine new model levels and compute error deltas.
			if self.mode == self.default_switching_mode_up:
				if level > 1:
					new_level = level - 1
					eta_m_new = self.error_measure_instance.get_model_error(pipe, new_level)
					delta_eta_m = eta_m - eta_m_new
					if delta_eta_m > self.eps:
						in_candidates = True
					else:
						new_level = 1
						eta_m_new = self.error_measure_instance.get_model_error(pipe, new_level)
						delta_eta_m = eta_m - eta_m_new
						if delta_eta_m > self.eps:
							in_candidates = True
				else:
					new_level = 1
					delta_eta_m = 0
			else:
				new_level = np.min([level + 1, self.max_level])
				eta_m_new = self.error_measure_instance.get_model_error(pipe, new_level)
				delta_eta_m = eta_m_new - eta_m
				if delta_eta_m < self.tau*self.eps:
					in_candidates = True

			#print(str(pipe) + " " + str(level) + " " + str(new_level) + " " + str(eta_m) + " " + str(eta_m_new))

			self.candidates.append((pipe, in_candidates, delta_eta_m, new_level))
		
	def compute_discretization_error(self):

		# Compute discretization error and get no of gridpoints.
		# Store data as a tuple of size 4 for each pipe:
		# 	(pipe, in_candidates, eta_d, no_gridpoints)
		#	in_candidates: boolean to indicate if pipe is in the set of up/down-switching candidates
		#	eta_d: discretization error
		#	no_gridpoints: number of gridpoints
		# (This is done in order to store all computed information.)

		self.candidates = []

		# Iterate over each pipe to find candidates, new model levels and error deltas.
		for pipe in self.pipes:

			no_gridpoints = self.get_no_gridpoints(pipe)
			level = self.get_model_level(pipe)

			# Don't coarsen pipes to smaller no. of grid points than M_evaluation.
			M0 = self.refinement_rule(self.error_measure_instance.M_evaluation)
			if no_gridpoints == M0 and self.mode == self.default_switching_mode_coarsen:
				in_candidates = False
				eta_d = None
				self.candidates.append((pipe, in_candidates, eta_d, no_gridpoints))
				continue
			if level == 3:
				in_candidates = False
				eta_d = None
				self.candidates.append((pipe, in_candidates, eta_d, no_gridpoints))
				continue
			
			# Indicates if pipe is in set A_{pi}^{>\eps} or A_{pi}^{<\eps}.
			in_candidates = True

			# Get discretization error estimates.
			eta_d = self.error_measure_instance.get_discretization_error(pipe)
			self.candidates.append((pipe, in_candidates, eta_d, no_gridpoints))

	def compute_switching_set(self):

		# Solve inequality from paper to find set U, D, R, C.
		# Computes a tuple of size 3 for each pipe set U, D, R or C:
		# 	(pipe, delta_eta_m, new_level OR no_gridpoints)
		#	delta_eta_m: model error difference
		#	new_level: new pipe model level
		#	no_gridpoints: number of gridpoints

		if self.mode == self.default_switching_mode_up:
			threshold = self.Theta_U
		elif self.mode == self.default_switching_mode_down:
			threshold = self.Theta_D
		elif self.mode == self.default_switching_mode_refine:
			threshold = self.Theta_R
		elif self.mode == self.default_switching_mode_coarsen:
			threshold = self.Theta_C # -Theta_C due to the >= relation

		# Find set of pipes from candidates, error deltas and threshold.
		error_deltas = np.array([c[2] for c in self.candidates if c[1] == True])
		saved_pipe_data = [(c[0],c[3]) for c in self.candidates if c[1] == True]
		candidates_length = error_deltas.size
		the_set = []

		# If the candidate set is non-empty apply the selection step 
		if candidates_length > 0:

			# If up or refinement
			if (self.mode == self.default_switching_mode_up or self.mode == self.default_switching_mode_refine):
				sorted_index = np.argsort(-error_deltas)
				sorted_error_deltas = error_deltas[sorted_index]
				sorted_error_deltas_cumsum = np.cumsum(sorted_error_deltas)
				max_threshold = threshold*sorted_error_deltas_cumsum[-1]
				index = 0
				the_set = [saved_pipe_data[sorted_index[index]]]
				while sorted_error_deltas_cumsum[index] < max_threshold:
					index = index + 1
					the_set.append(saved_pipe_data[sorted_index[index]])
				# print("")
				# print(the_set)
				# print("")

			# If down or coarsening
			else:
				sorted_index = np.argsort(error_deltas)
				sorted_error_deltas = error_deltas[sorted_index]
				sorted_error_deltas_cumsum = np.cumsum(sorted_error_deltas)
				max_threshold = threshold*sorted_error_deltas_cumsum[-1]
				the_set_index = sorted_index[max_threshold >= sorted_error_deltas_cumsum]
				the_set = [saved_pipe_data[a] for a in the_set_index]
				# if self.mode == self.default_switching_mode_down:
				# 	print(error_deltas)
				# 	print(sorted_error_deltas_cumsum)
				# 	print(max_threshold)
				# print("")
				# print(the_set)
				# print("")

		# Save the set
		if self.mode == self.default_switching_mode_up:
			self.U = the_set
		elif self.mode == self.default_switching_mode_down:
			self.D = the_set
		elif self.mode == self.default_switching_mode_refine:
			self.R = the_set
		elif self.mode == self.default_switching_mode_coarsen:
			self.C = the_set

		# Save the set size
		self.sets_sizes[self.mode].append(len(the_set))

	def apply_changes(self):

		#if up or down switching visited
		if self.visited_switching_mode[self.default_switching_mode_up]:
			Set = self.U
		if self.visited_switching_mode[self.default_switching_mode_down]:
			Set = self.D

		# Update pipe model levels.
		Set.sort()
		for (pipe, new_level) in Set:

			if self.print_model_grid_changes:
				self.print_model_changes(pipe, self.get_model_level(pipe), new_level)

			self.network.set_pipe_level(pipe, new_level)

		#if refinement or coarsening visited
		if self.visited_switching_mode[self.default_switching_mode_refine]:
			Set = self.R
			rule = self.refinement_rule
		if self.visited_switching_mode[self.default_switching_mode_coarsen]:
			Set = self.C
			rule = self.coarsening_rule

		# Update pipe grids.
		Set.sort()
		for (pipe, M) in Set:

			M_new = rule(M)

			if self.print_model_grid_changes:
				self.print_grid_changes(pipe, M, M_new)

			self.network.set_pipe_M(pipe, M_new)

		#Reinitialize the flags for visiting one of the modes
		self.visited_switching_mode = {self.default_switching_mode_up:False, self.default_switching_mode_down:False,
					       self.default_switching_mode_refine:False, self.default_switching_mode_coarsen:False} 

	def get_model_level(self, pipe):

		return self.pipe_data[pipe]['level']

	def get_no_gridpoints(self, pipe):

		return self.pipe_data[pipe]['M']
	
	def get_sets_sizes(self):

		return self.sets_sizes
	
	def set_network(self, network):

		# Set network data.
		
		if isinstance(network, dhn):
			self.network = network
			self.pipe_data = network.edges
			self.pipes = network.get_pipes_ids()
			
		else:
			self.network = None
			self.pipe_data = None
			self.pipes = None
			raise ValueError("Invalid network argument value. Valid value: dhn.")
	
	def set_switching_mode(self, mode):
		
		# Set the current switching mode.
		
		if mode == self.default_switching_mode_up:
			self.mode = self.default_switching_mode_up
			self.visited_switching_mode[mode] = True
		elif mode == self.default_switching_mode_down:
			self.mode = self.default_switching_mode_down
			self.visited_switching_mode[mode] = True
		elif mode == self.default_switching_mode_refine:
			self.mode = self.default_switching_mode_refine
			self.visited_switching_mode[mode] = True
		elif mode == self.default_switching_mode_coarsen:
			self.mode = self.default_switching_mode_coarsen
			self.visited_switching_mode[mode] = True
		else:
			self.mode = None
			raise ValueError("Invalid switching mode value. Valid mode values: up, down, refine, coarsen.")
	
	def set_computational_parameters(self, parameters):
		
		# Set computational parameters.

		self.eps = parameters['tolerance']
		self.mu = parameters['mu']
		self.tau = parameters['tau']
		self.Theta_U = parameters['Theta_U']
		self.Theta_D = parameters['Theta_D']
		self.Theta_R = parameters['Theta_R']
		self.Theta_C = parameters['Theta_C']

	def set_error_measure(self, error_measure_instance):

		if isinstance(error_measure_instance, error_estimate):
			self.error_measure_instance = error_measure_instance
		elif isinstance(error_measure_instance, exact_error):
			self.error_measure_instance = error_measure_instance
		else:
			raise ValueError('Invalid value for error_measure_instance: Valid values: error_estimate, exact_error')

	def print_grid_changes(self, pipe, M, M_new):

		if M_new > M:
			print(str(pipe) + " refined: " + str(M) + " -> " + str(M_new))
		else:
			print(str(pipe) + " coarsened: " + str(M) + " -> " + str(M_new))

	def print_model_changes(self, pipe, level, new_level):

		if new_level < level:
			print(str(pipe) + " switched up: " + str(level) + " -> " + str(new_level))
		else:
			print(str(pipe) + " switched down: " + str(level) + " -> " + str(new_level))
