from error_measure import *
import numpy as np
import scipy.optimize
import scipy.integrate
from scipy.interpolate import interp1d

class error_estimate(error_measure):

    # Class to compute model error estimates based on <optimization_problem>- and <dhn>-class.
    # Inherits from <error_measure>-class.

    def __init__(self, network_data, parameters, computational_parameters):
        super().__init__(network_data, parameters, computational_parameters)

        # Flags.
        self.coarser_grid = True

        # What model we are in atm
        self.valid_mode_model = "model"
        self.valid_mode_discretization = "discretization"
        self.valid_mode_NLP = "NLP"
        self.mode = None

        # Do we print the model info
        self.print_get_approximate_solution_info = False

        # Rule for number of gridpoints for coarser grid.
        self.rule = lambda M: int(0.5*(M - 1) + 1)

    def load_solution(self,energy_solution,velocity_solution):
        super().load_solution(energy_solution,velocity_solution)
        self.approximate_solution = {pipe: {} for pipe in self.pipes_set}

    def compute_model_error(self, pipe, model_level):

        self.mode = self.valid_mode_model

        if model_level == 1:

            # No model error per definition.
            eta_m = 0.0

        else:

            # Get approximate solutions.
            finest_model = self.get_approximate_solution(pipe, 1)
            current_model = self.get_approximate_solution(pipe, model_level)

            # Evaluate at reference grid.
            finest_model = self.evaluate_at_reference_grid(finest_model)
            current_model = self.evaluate_at_reference_grid(current_model)
            # print(finest_model-current_model)
            # print("")

            # Compute eta_a^m(y).
            eta_m = np.linalg.norm(finest_model - current_model, np.inf)
            
        # Return eta_a^m(y).
        return eta_m

    def compute_discretization_error(self, pipe):

        self.mode = self.valid_mode_discretization
    
        # Get approximate solutions.
        model_level = self.network_data.edges[pipe]["level"]
        solution = self.get_approximate_solution(pipe, model_level)
        coarser_solution = self.get_approximate_solution(pipe, model_level, self.coarser_grid)

        # Evaluate at reference grid.
        solution = self.evaluate_at_reference_grid(solution)
        coarser_solution = self.evaluate_at_reference_grid(coarser_solution)

        # if len(coarser_solution) < 3:
        #     print(solution)
        #     print(coarser_solution)

        # Compute & return eta_a^d(y).
        return np.linalg.norm(solution - coarser_solution, np.inf)

    def compute_total_error(self, pipe):
        pipe_level = self.network_data.get_pipe_level(pipe)
        discretization_error = self.compute_discretization_error(pipe)
        model_error = self.compute_model_error(pipe,pipe_level)
        return (discretization_error + model_error)
        

    def get_approximate_solution(self, pipe, requested_level, coarser_grid=False):

        # Get either M or coarser M.
        requested_M = self.network_data.get_pipe_M(pipe)
        if coarser_grid:
            requested_M = self.rule(requested_M)

        # Check if approximate solution already exists, if not compute it first.
        if not self.is_approximate_solution_computed(pipe, requested_level, requested_M):
            
            # Compute approximate solution for each grid point of pipe.
            self.approximate_solution[pipe][requested_level][requested_M]["solution"] = self.solve(pipe, requested_level, requested_M)

            if self.print_get_approximate_solution_info:
                mode = self.approximate_solution[pipe][requested_level][requested_M]["mode"]
                print("computing solution: " + str(pipe) + " " + str(requested_level) + " " + str(requested_M) + " ("  + str(mode) + ")")

        else:

            if self.print_get_approximate_solution_info:
                mode = self.approximate_solution[pipe][requested_level][requested_M]["mode"]
                print("loading solution:   " + str(pipe) + " " + str(requested_level) + " " + str(requested_M) + " ("  + str(mode) + ")")


        # Validate ode solution by comparing it to NLP solution.
        #self.validate_solution(pipe, requested_level, requested_M)

        return self.approximate_solution[pipe][requested_level][requested_M]["solution"]

    def is_approximate_solution_computed(self, pipe, requested_level, requested_M):

        # First check if NLP-solution already has requested solution.
        pipe_level, pipe_M = self.network_data.get_pipe_level(pipe), self.network_data.get_pipe_M(pipe)
        NLP_solution = None
        NLP_has_solution = False
        if pipe_level == requested_level and pipe_M == requested_M:
            NLP_has_solution = True
            NLP_solution = np.array([self.energy_solution[(pipe, j)] for j in range(pipe_M)])

        # Get potential available levels of pipe.
        levels = np.array(self.approximate_solution[pipe].keys())

        # Check if any level exists.
        if np.any(levels == requested_level):

            # Get potential available Ms for level of pipe.
            Ms = np.array(list(self.approximate_solution[pipe][requested_level].keys()))

            # Check if any M exists.
            if np.any(Ms == requested_M):

                # Approximate solution is computed for level and M.
                return True

            else:

                # If existend, add NLP solution to <approximate_solution>.
                if NLP_has_solution:
                    self.approximate_solution[pipe][pipe_level][pipe_M] = {"solution": NLP_solution, "mode": self.valid_mode_NLP}
                    return True

                # Approximate solution is computed for level and NOT for M.
                # Already create new key/value pair for new to be computed solution.
                self.approximate_solution[pipe][requested_level][requested_M] = {"solution": None, "mode": "get " + self.mode + " error"}
                return False

        else:

            # If existend, add NLP solution to <approximate_solution>.
            if NLP_has_solution:
                self.approximate_solution[pipe][pipe_level] = {pipe_M: {"solution": NLP_solution, "mode": self.valid_mode_NLP}}
                return True

            # Approximate solution is computed NOT for level and NOT for M.
            # Already create new key/value pair for new to be computed solution.
            self.approximate_solution[pipe][requested_level] = {requested_M: {"solution": None, "mode": "get " + self.mode + " error"}}
            return False

    def solve(self, pipe, model_level, M):

        # Create variables for model parameters (for clarity of code).
        v_a = self.velocity_solution[pipe] # velocity
        rho_a = self.parameters["rho"] # mass density
        L_a = self.network_data.edges[pipe]["length"] # pipe length
        D_a = self.network_data.edges[pipe]["diameter"] # pipe diameter
        r = self.network_data.edges[pipe]["roughness"] # pipe wall roughness
        lambda_a = np.power(2.0*np.log10(D_a/r) + 1.138, -2) # pipe friction coefficient
        k_W = self.network_data.edges[pipe]["ht_coefficient"] # heat transfer coefficient
        T_W = self.parameters["temp_ground"] + 273.0 # pipe wall temperature
        theta = self.theta_state_equation # temperature-energy equation of state coefficients
        e_0 = self.e0 # reference energy for equation of state
        
        # Compute grid size.
        dx = L_a/(M - 1)

        if model_level == 1 or model_level == 2:

                # Define constants for quadratic equation solver.
                c1 = v_a/dx
                c2 = - lambda_a*rho_a*abs(v_a)*(v_a**2)/2/D_a
                c3 = 4*k_W/D_a
                c4 = theta[2]/4/(e_0**2)
                c5 = theta[1]/2/e_0
                c6 = theta[0] - T_W
                if model_level == 1:
                    d0 = lambda x0: -c1*x0 + c2 + c3*c6 + c3*c5*x0 + c3*c4*(x0**2)
                elif model_level == 2:
                    d0 = lambda x0: -c1*x0 + c3*c6 + c3*c5*x0 + c3*c4*(x0**2) # just c2 = 0
                d1 = lambda x0: c1 + c3*c5 + 2*c3*c4*x0
                d2 = c3*c4

                x1 = lambda x0: (-d1(x0) + np.sqrt(d1(x0)**2 - 4*d0(x0)*d2))/2/d2
                x2 = lambda x0: (-d1(x0) - np.sqrt(d1(x0)**2 - 4*d0(x0)*d2))/2/d2

                # Solve quadratic equation for each grid point.
                E = np.zeros(M)
                E[0] = self.energy_solution[(pipe, 0)]
                for k in range(1,M):

                    e1, e2 = x1(E[k-1]), x2(E[k-1])
                    imaginary_sqrt = d1(E[k-1])**2 - 4*d0(E[k-1])*d2 < 0
                    if e1 > 0 and e2 < 0 and not imaginary_sqrt:
                        E[k] = e1 # this is the common case
                    elif e1 < 0 and e2 > 0 and not imaginary_sqrt:
                        E[k] = e2 # this has never been observed
                    else:
                        print("k = %d, x1 = %1.4e, x2 = %1.4e, imaginary sqrt: %r" % (k, e1, e2, imaginary_sqrt))
                        raise ValueError("Energy solution of quadratic equation is negative or complex.")

        else:

            # Compute linearly interpolated energies based on NLP solution.
            E = self.energy_solution[(pipe, 0)]*np.ones(M)

        return E

    def implicit_euler(f, y0, dx, I):

        raise NotImplementedError

        E = np.zeros((I[1]-I[0])/(dx + 1))
        E[0] = y0
        k = 0
        for x in np.arange(I[0], [1], dx):
            k += 1
            E[k] = E[k-1] + f(x,y)

        return E

    def validate_solution(self, pipe, requested_level, requested_M):

        # Only do validation if the current solution comes from NLP.
        if self.approximate_solution[pipe][requested_level][requested_M]["mode"] == self.valid_mode_NLP:
            E_SOLVE = self.solve(pipe, requested_level, requested_M)
            E_NLP = self.approximate_solution[pipe][requested_level][requested_M]["solution"]
            rel = 1 - E_SOLVE/E_NLP
            print("comparing ODE to NLP solution: max. norm of rel. errors 1 - solve/NLP: %1.4e" % (np.linalg.norm(rel, np.inf),))

    def evaluate_at_reference_grid(self, values):

        # Returns a subset of values for indices 0, step, 2*step, ..., M.

        # Current number of grid points/number of gridpoints from reference/evaluation grid.
        M, M0 = len(values), self.M_evaluation

        # Return appropriate grid points.
        if M0 < M:
            step = int((M-1)/(M0-1))
            return values[np.arange(0, M, step)]
        else:
            return values

    def print_approximate_solution(self):

        self.pipes_set.sort()

        for pipe in self.pipes_set:

            # Get model levels.
            levels = list(self.approximate_solution[pipe].keys())
            if len(levels) > 0:
                print("pipe " + str(pipe) + ":") # print pipe id as header
                levels.sort()
                for level in levels:

                    print("\tlvl = " + str(level) + ":") # print pipe model level
                    Ms = list(self.approximate_solution[pipe][level].keys())
                    if len(Ms) > 0:
                        Ms.sort()
                        for M in Ms:
                            print("\t\tM = %d (%s)" % (M, self.approximate_solution[pipe][level][M]["mode"])) # print number of grid points and current mode
