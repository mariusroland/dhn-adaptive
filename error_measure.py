import numpy as np
import time

class error_measure:

    def __init__(self, network_data, parameters, computational_parameters):
		
        #Class attributes (definable)
        self.network_data = network_data
        self.parameters = parameters
        self.computational_parameters = computational_parameters
        self.pipes_set =  self.network_data.get_pipes_ids()
        self.theta_state_equation = [274.93729, 220.536, 59.2453]
        self.e0 = 1e9
        self.M_evaluation = self.computational_parameters["M_evaluation"]
        
        self.model_error = {}
        self.discretization_error = {}
        self.total_error = {}
        self.is_discretiatzion_error_computed = {} # {"pipe_id": True/False}
        self.is_model_error_computed = {} # {"pipe_id": True/False}
        self.is_total_error_computed = {}

        #Class attributes (non definable)
        self.energy_solution = None
        self.velocity_solution = None

    def load_solution(self, energy_solution, velocity_solution):

        print("resetting solution data structure ...")

        # Save new solution.
        self.energy_solution = energy_solution
        self.velocity_solution = velocity_solution

        # Initialize the dictionaries so nothing is computed twice.
        self.is_discretization_error_computed = {e:False for e in self.pipes_set}
        self.is_model_error_computed = {(e,l):False for e in self.pipes_set for l in range(1,4)}
        self.is_total_error_computed = {e:False for e in self.pipes_set}

        # If the velocity is negative we flip the pipe's discretization.
        for pipe_id in self.network_data.get_pipes_ids():
            if self.velocity_solution[pipe_id] < 0:
               self.velocity_solution[pipe_id] = -self.velocity_solution[pipe_id]
               M = self.network_data.get_pipe_M(pipe_id)
               saved_energy_solution = {(pipe_id,index):self.energy_solution[(pipe_id,index)] for index in range(M)}
               for index in range(M):
                   self.energy_solution[(pipe_id,index)] = saved_energy_solution[(pipe_id,M-1-index)]

    def get_model_error(self, pipe_id, requested_model_level):
        if not self.is_model_error_computed[(pipe_id, requested_model_level)]:
            self.model_error[(pipe_id, requested_model_level)] = self.compute_model_error(pipe_id, requested_model_level)
            self.is_model_error_computed[(pipe_id, requested_model_level)] = True
        return self.model_error[(pipe_id, requested_model_level)]        

    def get_discretization_error(self,pipe_id):    
        if not self.is_discretization_error_computed[pipe_id]:
            self.discretization_error[pipe_id] = self.compute_discretization_error(pipe_id)
            self.is_discretization_error_computed[pipe_id] = True            
        return self.discretization_error[pipe_id]

    def get_total_error(self,pipe_id):
        if not self.is_total_error_computed[pipe_id]:
            self.total_error[pipe_id] = self.compute_total_error(pipe_id)
            self.is_total_error_computed[pipe_id] = True            
        return self.total_error[pipe_id]

    def get_network_discretization_error(self):
        network_sum = 0.0
        for pipe_id in self.network_data.get_pipes_ids():
            network_sum = network_sum + self.get_discretization_error(pipe_id)
        return network_sum

    def get_network_model_error(self):
        network_sum = 0.0
        for pipe_id in self.network_data.get_pipes_ids():
            network_sum = network_sum + self.get_model_error(pipe_id, self.network_data.get_pipe_level(pipe_id))
        return network_sum
    
    def get_network_total_error(self):
        network_sum = 0.0
        for pipe_id in self.network_data.get_pipes_ids():
            network_sum = network_sum + self.get_total_error(pipe_id)
        return network_sum
