from optimization_problem import *
from exact_error import *
from error_estimate import *
from exact_error import *
from switching_strategy import *
from matplotlib.ticker import MaxNLocator
import matplotlib.font_manager
import tikzplotlib
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb}')
#plt.rc('pgf', preamble=r'\usepackage{amsmath} \usepackage{amssymb}')
#plt.rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
plt.rc('font', size=22)

class adaptive_algorithm:

    def __init__(self,system_data,scenario_data,parameters,computational_parameters):

        # Saving all the instance data
        self.system_data = system_data
        self.scenario_data = scenario_data
        self.parameters = parameters
        self.computational_parameters = computational_parameters

        # Flags.
        self.valid_error_measure_exact = "exact"
        self.valid_error_measure_estimate = "estimate"

        # Options for plotting.
        self.energy_scaling = {"factor": 1e-9, "unit": 'GJ'} # for plotting
        self.plot_network_file = None

        # Creating the objects used in the algorithm
        self.network = dhn(self.system_data, self.scenario_data, self.computational_parameters)
        self.optimizaton_problem = optimization_problem(self.network,self.parameters,self.computational_parameters)
        if self.is_error_measure_exact():
            self.error = exact_error(self.network, self.parameters, self.computational_parameters)
        elif self.is_error_measure_estimate():
            self.error = error_estimate(self.network, self.parameters, self.computational_parameters)
        else:
            raise ValueError("Invalid entry for error measure")

        print("Selected error measure: " + self.computational_parameters["measure"])
        self.switching_strategy = switching_strategy(network=self.network,computational_parameters=self.computational_parameters,
                                                     error_measure_instance=self.error)

        # Getting the relevant parameters
        self.epsilon = self.computational_parameters["tolerance"]
        self.k_max = self.computational_parameters["k_max"] # max. number of outer loop iterations
        self.mu = self.computational_parameters["mu"] # max. number of inner loop iterations

        # Computational information
        self.is_inner_loop_iteration = True
        self.iteration_counter = 0
        self.iteration_values = {"inner":[],"outer":[]}
        self.average_error_values = {"discretization":[], "model":[], "total":[]}
        self.all_Ms = []
        self.model_level_sets_sizes = {1:[], 2:[], 3:[]}
        
        # Initializing the objects in order to be ready for the algorithm
        self.network.flow_direction_presolve()
        self.optimizaton_problem.setup_intance()

    def save_error_information(self):

        # Get current average network errors.
        number_of_pipes = self.network.get_len_pipes()
        self.average_error_values["model"].append(self.error.get_network_model_error()/number_of_pipes)
        self.average_error_values["discretization"].append(self.error.get_network_discretization_error()/number_of_pipes)
        self.average_error_values["total"].append(self.error.get_network_total_error()/number_of_pipes)

    def save_discretization_information(self):

        # Get all the Ms
        data = [self.network.get_pipe_M(pipe_id) for pipe_id in self.network.get_pipes_ids()]

        #Then add them to the list
        self.all_Ms.append(data)

    def save_model_level_sets_sizes(self):

        # Get model level sets sizes
        self.model_level_sets_sizes[1].append(0)
        self.model_level_sets_sizes[2].append(0)
        self.model_level_sets_sizes[3].append(0)        
        for pipe_id in self.network.get_pipes_ids():
            self.model_level_sets_sizes[self.network.get_pipe_level(pipe_id)][-1] = self.model_level_sets_sizes[self.network.get_pipe_level(pipe_id)][-1] + 1

    def save_iteration_counter(self):
        
        if self.is_inner_loop_iteration and self.iteration_counter > 0:
            self.iteration_values["inner"].append(self.iteration_counter)
        elif not self.is_inner_loop_iteration:
            self.iteration_values["outer"].append(self.iteration_counter)
        self.iteration_counter = self.iteration_counter + 1

        
    def save_computational_information(self):

        self.save_error_information()
        self.save_discretization_information()
        self.save_model_level_sets_sizes()
        self.save_iteration_counter()
        

    def rescale_list_with_pipes_length(self,list_name):
        number_of_pipes = self.network.get_len_pipes()        
        return [entry/number_of_pipes*100 for entry in list_name]

    def is_error_measure_exact(self):

        return self.computational_parameters["measure"] == self.valid_error_measure_exact

    def is_error_measure_estimate(self):

        return self.computational_parameters["measure"] == self.valid_error_measure_estimate

    def first_solve(self):

        print("solving network for the first time ...")
        self.optimizaton_problem.setup_intance()
        self.optimizaton_problem.initial_warmstart()
        #self.plot_network(True,"ntm")
        #input()
        self.optimizaton_problem.solve()

    def loop_solve(self):

        print("solving network ...")
        self.optimizaton_problem.reconstruct()
        self.optimizaton_problem.solve()

    def is_epsilon_feasible(self):

        print("Feasibility check:")

        # Load the new solution inside of the error measure
        self.error.load_solution(self.optimizaton_problem.get_internal_energy_from_solution(), self.optimizaton_problem.get_velocity_from_solution())

        # Save the computational information
        self.save_computational_information()

        # Feasibility check.
        feasible = self.average_error_values["total"][-1] <= self.epsilon

        # Print average network error.
        me, de, te = self.average_error_values["model"][-1], self.average_error_values["discretization"][-1], self.average_error_values["total"][-1]
        if feasible:
            print("error %1.4e < tolerance %1.4e, me: %1.4e, de: %1.4e" % (te, self.epsilon, me, de))
        else:
            print("error %1.4e > tolerance %1.4e, me: %1.4e, de: %1.4e" % (te, self.epsilon, me, de))

        # Return feasibility check.
        return feasible

    def inner_loop_switching(self):
        self.switching_strategy.compute_grid_refinement_set()
        self.switching_strategy.compute_model_upswitching_set()
        self.switching_strategy.apply_changes()

    def outer_loop_switching(self):
        self.switching_strategy.compute_grid_coarsening_set()
        self.switching_strategy.compute_model_downswitching_set()
        self.switching_strategy.apply_changes()
        
    def apply_adaptive_algorithm(self,plot_network_flag=False):

        if len(self.network) > 0:
            print("\nApplying Adaptive Algorithm to network: " + self.network.name)
        else:
            print("\nApplying Adaptive Algorithm")

        self.first_solve()
        self.tikz_plot_network(plot_network_flag,"first")

        if self.is_epsilon_feasible():
            return
        for k in range(self.k_max):

            for j in range(self.mu):
                
                print("\nRefining & up-switching iteration: %d/%d" % (j+1, self.mu))
                self.is_inner_loop_iteration = True
                self.inner_loop_switching()
                self.loop_solve()
                
                if self.is_epsilon_feasible():
                    self.tikz_plot_network(plot_network_flag,"last")
                    return

            print("\nCoarsening & down-switching iteration: %d/%d" % (k+1, self.k_max))

            self.is_inner_loop_iteration = False
            self.outer_loop_switching()
            self.loop_solve()
            
            if self.is_epsilon_feasible():
                self.tikz_plot_network(plot_network_flag,"last")
                return

        print("\nAdaptive Algorithm did not converge. Maximum number of outer loop iterations reached.")

    def set_plot_file(self,file_name):
        self.plot_network_file = file_name

    def plot_network(self,plot_flag,iteration_location):
        if plot_flag:
            if self.plot_network_file != None:
                self.optimizaton_problem.plot_node_results(plot_network_file=self.plot_network_file+"_"+iteration_location)
            else:
                self.optimizaton_problem.plot_node_results()

    def tikz_plot_network(self,plot_flag,iteration_location):
        if (self.system_data["name"] == "Aroma" and plot_flag):
            file_name_nodes = "/home/marius/Documents/trier/dhn/dhn-adaptive/code/src/data_extraction/node_sol.tikz"
            file_name_edges = "/home/marius/Documents/trier/dhn/dhn-adaptive/code/src/data_extraction/edge_sol.tikz"
            final_output = "/home/marius/Documents/trier/dhn/dhn-adaptive/paper/figures/tikz/aroma_visu_"
            self.optimizaton_problem.save_results(file_name_nodes,file_name_edges,final_output+iteration_location+".tikz")

    def plot_error_values(self,file_name = None):
        
        # Preparing plot.
        fig, ax = plt.subplots()
        ax.set_yscale('log')

        # Scaling for energies.
        Giga = self.energy_scaling['factor']

        # Line style.
        linestyle = {"linestyle": "--", "marker": "o"}
        if file_name != None:
            used_markersize = 3.5
        else:
            used_markersize = 5
        
        # Extract the values that are zero and remove everything afterwards
        saved_model_index = len(self.average_error_values['model'])
        for iteration in range(len(self.average_error_values['model'])):
            if self.average_error_values['model'][iteration] == 0:
                saved_model_index = iteration
                break

        # Preparing x and y coordinates.
        x_model = list(range(0,saved_model_index))
        x_total = list(range(0,len(np.array(self.average_error_values['total'])*Giga)))
        if file_name != None:
            x_discretization = list(range(0,len(self.average_error_values['discretization'])))
            average_discretization_error = self.average_error_values['discretization']
        else:
            x_discretization = list(range(1,len(self.average_error_values['discretization'])))
            average_discretization_error = self.average_error_values['discretization'][1:]

        # Creating plot
        if self.is_error_measure_exact():
            error_type_string = "\\nu"
            y_label = 'Exact error ('
        else:
            error_type_string = "\\eta"
            y_label = 'Error estimate ('
        ax.plot(x_discretization, np.array(average_discretization_error)*Giga, markersize=used_markersize, label = r"$"+error_type_string+"^{\\text{d}}$", **linestyle)        
        ax.plot(x_model, np.array(self.average_error_values['model'])[0:saved_model_index]*Giga, markersize=used_markersize, label = r"$"+error_type_string+"^{\\text{m}}$", **linestyle)
        ax.plot(x_total, np.array(self.average_error_values['total'])*Giga, markersize=used_markersize, label = r"$"+error_type_string+"$", **linestyle)
        ax.set(xlabel='Iteration', ylabel= y_label + self.energy_scaling['unit'] + ')')
        ax.xaxis.set_major_locator(MaxNLocator(integer=True))
        ax.set_ylim([10**(-7),10**(-1.5)])
        ax.legend()
        ax.grid()

        # Saving or displaying
        if file_name != None:
            tikzplotlib.save(file_name + "_error_values.pgf", encoding='utf-8')
            #fig.savefig(file_name + "_error_values.pgf", bbox_inches='tight')
        else:
            fig.show()

    def plot_inner_loop_sets(self,file_name = None):
        
        # Preparing plot
        fig, ax = plt.subplots()

        # Line style.
        if file_name != None:
            used_markersize = 3.5
        else:
            used_markersize = 5

        # Preparing x and y coordinates
        sets_sizes = self.switching_strategy.get_sets_sizes()
        x_up = self.iteration_values["inner"]
        x_refinement = self.iteration_values["inner"]

        # Plotting
        ax.plot(x_up,self.rescale_list_with_pipes_length(sets_sizes["up"]), linestyle='--', marker='o', markersize=used_markersize, label = r"$|\mathcal{U}|$")     
        ax.plot(x_refinement,self.rescale_list_with_pipes_length(sets_sizes["refine"]), linestyle='--', marker='o', markersize=used_markersize, label = r"$|\mathcal{R}|$")
        ax.set(xlabel='Iteration', ylabel=r'Proportion of pipes (\%)')
        ax.set_ylim([-10,110])
        ax.set_xlim(left = -1)
        ax.xaxis.set_major_locator(MaxNLocator(integer=True))
        #fig.gca().set_yticklabels(['{:.0f}%'.format(z*100) for z in fig.gca().get_yticks()]) 
        
        ax.legend()
        ax.grid()

        # Saving or displaying
        if file_name != None:
            tikzplotlib.save(file_name + "_inner_loop_sets.pgf", encoding='utf-8')
            #fig.savefig(file_name + "_inner_loop_sets.pgf",bbox_inches='tight')
        else:
            fig.show()  

    def plot_outer_loop_sets(self,file_name = None):
        
        # Preparing plot
        fig, ax = plt.subplots()

        # Line style.
        if file_name != None:
            used_markersize = 3.5
        else:
            used_markersize = 5

        # Preparing x and y coordinates
        sets_sizes = self.switching_strategy.get_sets_sizes()
        x_down = self.iteration_values["outer"]
        x_coarsening = self.iteration_values["outer"]
        
        # Creating plot
        ax.plot(x_down,self.rescale_list_with_pipes_length(sets_sizes["down"]), linestyle='--', marker='o', markersize=used_markersize, label = r"$|\mathcal{D}|$")     
        ax.plot(x_coarsening,self.rescale_list_with_pipes_length(sets_sizes["coarsen"]), linestyle='--', marker='o', markersize=used_markersize, label = r"$|\mathcal{C}|$")
        ax.set(xlabel='Iteration', ylabel=r'Proportion of pipes (\%)')
        ax.xaxis.set_major_locator(MaxNLocator(integer=True))
        ax.set_ylim([-10,110])
        ax.set_xlim(left = -1, right=(self.iteration_counter))
        ax.legend()
        ax.grid()

        # Saving or displaying
        if file_name != None:
            tikzplotlib.save(file_name + "_outer_loop_sets.pgf", encoding='utf-8')
            #fig.savefig(file_name + "_outer_loop_sets.pgf",bbox_inches='tight')
        else:
            fig.show()


    def plot_inner_outer_loop_sets(self,file_name = None):
        
        # Preparing plot
        fig, ax = plt.subplots()

        # Line style.
        if file_name != None:
            used_markersize = 3.5
        else:
            used_markersize = 5

        # Preparing x and y coordinates
        sets_sizes = self.switching_strategy.get_sets_sizes()
        x_up = self.iteration_values["inner"]
        x_refinement = self.iteration_values["inner"]
        x_down = self.iteration_values["outer"]
        x_coarsening = self.iteration_values["outer"]

        # Getting colors
        cmap = plt.get_cmap("tab10")
        
        # Plotting
        ax.plot(x_up,np.array(self.rescale_list_with_pipes_length(sets_sizes["up"])), linestyle='--', marker='o', markersize=used_markersize, label = r"$\mathcal{U}$")     
        ax.plot(x_refinement,np.array(self.rescale_list_with_pipes_length(sets_sizes["refine"])), linestyle='--', marker='o', markersize=used_markersize, label = r"$\mathcal{R}$")
        ax.plot(x_down,np.array(self.rescale_list_with_pipes_length(sets_sizes["down"])), linestyle='--', marker='o', markersize=used_markersize, label = r"$\mathcal{D}$")     
        ax.plot(x_coarsening,np.array(self.rescale_list_with_pipes_length(sets_sizes["coarsen"])), linestyle='--', marker='o', markersize=used_markersize, label = r"$\mathcal{C}$")
        ax.set(xlabel='Iteration', ylabel=r'Proportion of pipes (\%)')
        ax.set_ylim([-10,110])
        ax.set_xlim(left = -1)
        ax.xaxis.set_major_locator(MaxNLocator(integer=True))
        ax.legend()
        ax.grid()

        # Saving or displaying
        if file_name != None:
            tikzplotlib.save(file_name + "_inner_outer_loop_sets.pgf", encoding='utf-8')
            #fig.savefig(file_name + "_outer_loop_sets.pgf",bbox_inches='tight')
        else:
            fig.show()


    def plot_model_level_sets_sizes(self,file_name = None):
        
        # Preparing plot
        fig, ax = plt.subplots()

        # Preparing x and y coordinates
        x = list(range(0,len(self.model_level_sets_sizes[1])))

        # Line style.
        if file_name != None:
            used_markersize = 3.5
        else:
            used_markersize = 5

        # Creating plot
        self.rescale_list_with_pipes_length(self.model_level_sets_sizes[1])
        ax.plot(x,self.rescale_list_with_pipes_length(self.model_level_sets_sizes[1]), linestyle='--', marker='o', markersize=used_markersize, label = "M1")     
        ax.plot(x,self.rescale_list_with_pipes_length(self.model_level_sets_sizes[2]), linestyle='--', marker='o', markersize=used_markersize, label = "M2")
        ax.plot(x,self.rescale_list_with_pipes_length(self.model_level_sets_sizes[3]), linestyle='--', marker='o', markersize=used_markersize, label = "M3")         
        ax.set(xlabel='Iteration', ylabel=r'Proportion of pipes (\%)')
        ax.xaxis.set_major_locator(MaxNLocator(integer=True))
        ax.set_ylim([-10,110])
        ax.legend()
        ax.grid()

        # Saving or displaying
        if file_name != None:
            tikzplotlib.save(file_name + "_model_level_sets_sizes.pgf", encoding='utf-8')
            #fig.savefig(file_name + "_model_level_sets_sizes.pgf",bbox_inches='tight')
        else:
            fig.show()

    def plot_boxplot_Ms(self,file_name = None):
        
        # Preparing plot
        fig, ax = plt.subplots()
        ax.set_yscale('log')

        # Preparing x and y coordinates
        x = list(range(0,len(self.all_Ms)))

        # Line style.
        if file_name != None:
            used_markersize = 3.5
        else:
            used_markersize = 5

        # Creating plot
        ax.violinplot(self.all_Ms,positions=x)             
        ax.set(xlabel='Iteration', ylabel=r'Grid points')
        ax.set_xticks(np.arange(0, x[-1]+1, 3)) 
        #ax.xaxis.set_major_locator(MaxNLocator(integer=True))
        #ax.set_ylim([-10,110])
        #ax.legend()
        #ax.grid()

        # Saving or displaying
        if file_name != None:
            tikzplotlib.save(file_name + "_boxplot_M.pgf", encoding='utf-8')
            #fig.savefig(file_name + "_model_level_sets_sizes.pgf",bbox_inches='tight')
        else:
            fig.show() 

    def plot_solving_times(self,file_name = None):
        
        # Preparing plot
        fig, ax = plt.subplots()
        ax.set_yscale('log')

        # Line style.
        if file_name != None:
            used_markersize = 3.5
        else:
            used_markersize = 5

        # Preparing x and y coordinates
        nlp_solving_time = self.optimizaton_problem.get_nlp_solving_time()
        x_nlp = list(range(0,len(nlp_solving_time)))

        # Plotting
        ax.plot(x_nlp,nlp_solving_time, linestyle='--', marker='o', markersize=used_markersize)     
        ax.set(xlabel='Iteration', ylabel='Time (s)')
        ax.xaxis.set_major_locator(MaxNLocator(integer=True))
        ax.grid()

        # Saving or displaying
        if file_name != None:
            tikzplotlib.save(file_name + "_solving_times.pgf", encoding='utf-8')            
            #fig.savefig(file_name + "_solving_times.pgf",bbox_inches='tight')
        else:
            fig.show()
